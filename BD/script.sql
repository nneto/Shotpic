
DROP DATABASE if EXISTS projetS4;
CREATE DATABASE projetS4;
use projetS4;

Drop TABLE IF EXISTS `compte`;
CREATE TABLE compte(
        pseudo     Varchar (25) NOT NULL ,
        idcompte   int (11) Auto_increment  NOT NULL ,
        motDePasse Text ,
        email      Varchar (25) NOT NULL ,
        nom        Varchar (25) ,
        prenom     Varchar (25) ,
        admin      Bool ,
        modo       Bool ,
        activer    Bool ,
        PRIMARY KEY (idcompte ) ,
        UNIQUE (pseudo ,email )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Photo
#------------------------------------------------------------
Drop TABLE IF EXISTS `photo`;
CREATE TABLE photo(
        idPhoto    int (11) Auto_increment  NOT NULL ,
        nomPhoto   Varchar (25) ,
        titrePhoto Varchar (250) ,
        datePhoto  Date ,
        heurePhoto Time ,
        idcompte   Int ,
        PRIMARY KEY (idPhoto )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Commentaire
#------------------------------------------------------------
Drop TABLE IF EXISTS `commentaire`;
CREATE TABLE commentaire(
        idCommentaire int (11) Auto_increment  ,
        contenu       Text ,
        dateCom       Date ,
        heureCom      Time ,
        idcompte      Int NOT NULL ,
        idPhoto       Int NOT NULL ,
        PRIMARY KEY (idCommentaire )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: voter
#------------------------------------------------------------
Drop TABLE IF EXISTS `voter`;
CREATE TABLE voter(
        note     Int ,
        idcompte Int NOT NULL ,
        idPhoto  Int NOT NULL ,
        PRIMARY KEY (idcompte ,idPhoto )
)ENGINE=InnoDB;

ALTER TABLE Photo ADD CONSTRAINT FK_Photo_idcompte FOREIGN KEY (idcompte) REFERENCES Compte(idcompte) on DELETE CASCADE ;
ALTER TABLE Commentaire ADD CONSTRAINT FK_Commentaire_idcompte FOREIGN KEY (idcompte) REFERENCES Compte(idcompte) on DELETE CASCADE ;
ALTER TABLE Commentaire ADD CONSTRAINT FK_Commentaire_idPhoto FOREIGN KEY (idPhoto) REFERENCES Photo(idPhoto) on DELETE CASCADE ;
ALTER TABLE voter ADD CONSTRAINT FK_voter_idcompte FOREIGN KEY (idcompte) REFERENCES Compte(idcompte) on DELETE CASCADE ;
ALTER TABLE voter ADD CONSTRAINT FK_voter_idPhoto FOREIGN KEY (idPhoto) REFERENCES Photo(idPhoto) on DELETE CASCADE ;


DROP TABLE IF EXISTS `token`;

CREATE TABLE IF not EXISTS `token` (
        `token` VARCHAR (20) PRIMARY KEY ,
        `creation` DATETIME ,
        `expiration`  INT UNSIGNED

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into compte (pseudo, idcompte, motDePasse, email, nom, prenom, admin, modo, activer) values
        ('nneto',1,'jnoeMHtmM580U','nicolasxv3@gmail.com','neto','nicolas',0,0,1);
INSERT INTO photo (idPhoto, nomPhoto, titrePhoto, datePhoto, heurePhoto, idcompte)  VALUE
        (1,'chat1.jpg','Sheitan','2017-01-01' ,'20:01:01' ,1),
        (2,'chat2.jpg','chat de très gros plan','2017-01-01' ,'19:07:23' ,1),
        (3,'chat3.jpg','chat bo','2017-05-01' ,CURRENT_TIME ,1),
        (4,'chat4.jpg','chat dans son jacuzzi','2017-06-01' ,CURRENT_TIME ,1),
        (5,'chat5.jpg','chat bo de très gros plan','2016-01-01' ,CURRENT_TIME ,1),
        (6,'chat6.jpg','chatoune de très gros plan','2012-01-01' ,CURRENT_TIME ,1),
        (7,'r.jpg','petite chipie fais ça michtoneuse','2017-02-01' ,CURRENT_TIME ,1);

INSERT INTO voter (note, idcompte, idPhoto) VALUE
        (3,1,1) ,
        (5,1,2),
        (4,1,3),
        (1,1,4),
        (1,1,5),
        (1,1,6),
        (1,1,7);

INSERT into commentaire (contenu, dateCom, heureCom, idcompte, idPhoto) VALUE
        ('trop mignonne',curdate(),curtime(),1,2),
        ('<3<3',curdate(),curtime(),1,2);