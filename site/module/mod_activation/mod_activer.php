<?php

/**
 * Created by PhpStorm.
 * User: NICO
 * Date: 14/02/2017
 * Time: 00:43
 */
require_once('controleur_activer.php');
class ModActiver extends ModuleGenerique
{
    public function activer($token,$idcompte){
        $this->controleur= new ControleurActiver();
        $this->controleur->activer($token,$idcompte);

    }
}