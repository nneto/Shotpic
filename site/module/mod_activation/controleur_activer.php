<?php

/**
 * Created by PhpStorm.
 * User: NICO
 * Date: 14/02/2017
 * Time: 00:44
 */
require_once ('modele_activer.php');
    class ControleurActiver extends ControleurGenerique
{

    public function activer($token,$idcompte){
        $this->modele=new ModeleActiver();
        if($this->modele->getToken($token)){
            $this->modele->effacerToken($token);
            $this->modele->activer($idcompte);
            header('Location:index.php?module=accueil&err=activation%20effectuée%20');
        }else{
            header('Location:index.php?module=accueil&err=activation%20expirer');
        }


    }
}