<?php

/**
 * Created by PhpStorm.
 * User: NICO
 * Date: 14/02/2017
 * Time: 00:44
 */
class ModeleActiver extends ModeleGenerique
{
    public function activer($idcompte){

        $requete='update compte set activer=1 where idcompte=? ';
        $requete=self::$connexion->prepare($requete);
        $requete->execute(array($idcompte));
    }

}