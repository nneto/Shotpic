<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:52
 */
class VueClassement extends VueGenerique
{
    public function afficherClassement($classement){
        $this->titre = "classement";
        $this->Css = array("<link href='module/mod_classement/classement.css' rel='stylesheet'>");
        include("include/nav.php");
        $this->contenu .="  <div class=\"container\" id=\"central\">";
        foreach($classement as $photo){
            $this->contenu .=" <div class='col-md-12 classement' > <img src='source/$photo[nomPhoto]' id='' class='img-responsive col-md-5' /><div class='col-md-7 moyenne' ><div class='col-md-1'> <p class='note'>$photo[moyenne]</p></div><div class='stars col-md-1'> <span  >&#9734</span> </div> </div></div>";

        }
        $this->contenu .="</div>";
        include_once ('include/footer.php');
    }

}
