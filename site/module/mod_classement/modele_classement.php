<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:53
 */
class ModeleClassement extends ModeleGenerique
{


    public function getClassement(){
       	$requete=" select *,round(avg(note),2) as moyenne from voter inner join photo using (idPhoto) group by idPhoto order By moyenne desc;";
        $requete=self::$connexion->prepare($requete);
        $requete->execute();
        return $requete->fetchall(PDO::FETCH_ASSOC);
    }
}
