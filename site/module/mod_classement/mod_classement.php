<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:47
 */
require_once('controleur_classement.php');
class ModClassement extends ModuleGenerique
{


    /**
     * ModClassement constructor.
     */
    public function __construct()
    {
        $this->controleur= new ControleurClassement();
    }

    public function afficherClassement(){
        $this->controleur->afficherClassement();
    }
}