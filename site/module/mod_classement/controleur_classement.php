<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:52
 */
require_once('modele_classement.php');
require_once('vue_classement.php');
class ControleurClassement extends ControleurGenerique
{

    /**
     * ControleurClassement constructor.
     */
    public function __construct()
    {
        $this->modele= new ModeleClassement();
        $this->vue= new VueClassement();
    }

    public function afficherClassement(){
        $classement=$this->modele->getClassement();
        $this->vue->afficherClassement($classement);
    }
}
