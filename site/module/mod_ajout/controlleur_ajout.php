<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 16/02/2017
 * Time: 18:14
 */

require_once ('modele_ajout.php');

class ControleurAjoutPhoto extends ControleurGenerique
{


    /**
     * ControleurAjoutPhoto constructor.
     */
    public function __construct()
    {
        $this->modele= new ModeleAjoutPhoto();
    }

    public function ajoutPhoto(){


        $token=$this->modele->getToken(1200);


        if (!isset($_POST['titre']) || strlen($_POST['titre'])<=0 ) {
            header('Location:index.php?module=accueil&err=element%20manquant');
        }else{
            $mimetype = mime_content_type($_FILES['fichImagePhoto']['tmp_name']);

            $extension_upload = strtolower(  substr(  strrchr($_FILES['fichImagePhoto']['name'], '.')  ,1)  );
            $_SESSION['file']=$_FILES;
            $temp=explode(".",$_FILES['fichImagePhoto']['name']);
            $name= round(microtime(true)).'.'.end($temp);
            if ( !$_FILES['fichImagePhoto']['size'] < 500*1024 &&in_array($mimetype, array('image/jpeg', 'image/gif', 'image/png', 'image/jpg')) ){
                $this->modele->getPhotos($name,$_POST['titre']);
                $a=move_uploaded_file($_FILES['fichImagePhoto']['tmp_name'], "source/".$name);

                header('Location:index.php?module=accueil&err=erreur');
            }
        }
        header('Location:index.php?module=accueil&err=erazez');
    }
}
