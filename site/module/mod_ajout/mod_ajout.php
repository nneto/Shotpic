<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 16/02/2017
 * Time: 18:15
 */
require_once ('controlleur_ajout.php');
class ModuleAjoutPhoto extends ModuleGenerique{


    /**
     * ModuleAffichePhoto constructor.
     */
    public function __construct()
    {
        $this->controleur= new ControleurAjoutPhoto();
    }

    public function afficher(){
        $this->controleur->ajoutPhoto();
    }
}
