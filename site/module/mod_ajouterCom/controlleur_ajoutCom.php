<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 16/02/2017
 * Time: 18:14
 */

require_once('modele_ajoutCom.php');

class ControleurAjoutCom extends ControleurGenerique
{


    /**
     * ControleurAjoutPhoto constructor.
     */
    public function __construct()
    {
        $this->modele= new ModeleAjoutCom();
    }

    public function commenter($tab){
        $this->modele->commenter($tab);
        header("Location:index.php?module=detail&id=$tab[idphoto]");
    }
}
