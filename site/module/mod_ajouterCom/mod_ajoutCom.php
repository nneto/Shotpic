<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 16/02/2017
 * Time: 18:15
 */
require_once('controlleur_ajoutCom.php');
class ModuleAjoutCom extends ModuleGenerique{


    /**
     * ModuleAffichePhoto constructor.
     */
    public function __construct()
    {
        $this->controleur= new ControleurAjoutCom();
    }

    public function commenter($tab){
        $this->controleur->commenter($tab);
    }
}
