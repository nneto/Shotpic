<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 09/11/16
 * Time: 18:40
 */
include_once('controleur_del_Photo.php');
class ModDelPhoto extends ModuleGenerique
{
    public function module_delete($id){

        $this->controleur=new ControleurDelPhoto();
        $this->controleur->delPhoto($id);
    }


}