<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 09/11/16
 * Time: 18:42
 */
require_once('model_del_Photo.php');

class ControleurDelPhoto extends ControleurGenerique
{
    public function __construct()
    {
        $this->modele=new ModelDelPhoto();
    }

    public function delPhoto($id){
        $this->modele->delPhoto($id);
        header('Location:index.php?module=affiche');
    }
}