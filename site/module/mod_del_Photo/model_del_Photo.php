<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 09/11/16
 * Time: 18:44
 */
class ModelDelPhoto extends ModeleGenerique
{
    public function delPhoto($id){
        try{
            $requeteCompte='delete from photo where idPhoto=?';
            $requete=self::$connexion->prepare($requeteCompte);
            $requete->execute(array($id));
        }catch (PDOException $p){
            throw new ModeleInscriptionException();
        }
    }

}