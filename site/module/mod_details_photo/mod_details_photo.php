<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:47
 */
require_once ('controleur_details_photo.php');
class ModDetailPhoto extends ModuleGenerique
{

    /**
     * ModDetailPhoto constructor.
     */
    public function __construct()
    {
        $this->controleur= new ControleurDetailPhoto();
    }

    public function detail($idPhoto){
        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            $this->controleur->detail($idPhoto,$_SESSION['idcompte']);
        }else{
            $this->controleur->detail($idPhoto);
        }

    }
}