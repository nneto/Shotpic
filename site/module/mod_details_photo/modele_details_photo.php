<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:53
 */
class ModeleDetailsPhoto extends ModeleGenerique
{

    public function getDetail($id){
        $requete="select * from photo where idPhoto=?";
        $requete=self::$connexion->prepare($requete);
        $requete->execute(array($id));
        return $requete->FetchAll(PDO::FETCH_ASSOC);
    }

    public function getCom($id){
        $requete="select * from commentaire natural join compte where idPhoto=?";
        $requete=self::$connexion->prepare($requete);
        $requete->execute(array($id));
        return $requete->FetchAll(PDO::FETCH_ASSOC);
    }

    public function getVoter(){
   	$requete="select * from voter where idPhoto=? and idcompte=?";
        $requete=self::$connexion->prepare($requete);
        $requete->execute(array($_GET['id'],$_SESSION['idcompte']));
        return $requete->fetchall(PDO::FETCH_ASSOC);
    }
}
