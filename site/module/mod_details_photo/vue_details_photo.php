<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:52
 */
class VueDetailsPhoto extends VueGenerique
{

    public function vue_detail($detail, $com, $aVoter, $token)
    {

        $this->titre = $detail['titrePhoto'];
        $this->Css = array("<link href='module/mod_details_photo/detail_photo.css' rel='stylesheet'>");
        include ("include/nav.php");

        $detail['titrePhoto']=htmlspecialchars($detail['titrePhoto']);
        $this->contenu .= "
       
             <div class=\"container\" id=\"central\">
               
                <img class='center-block photo img-responsive' src='source/$detail[nomPhoto]'>
                     <h1 class='text-center'>$detail[titrePhoto]</h1>
                    <div class='col-md-12'>
                    
                    
                  ";

        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))) {
            if($_SESSION['admin']==0 && $_SESSION['modo']==0){
                if (!empty($aVoter)) {
                    $this->contenu .= "<div class='col-md-3'><div class='stars'>
	<span onclick='afficheBootbox2()'>&#9734</span>
	<span onclick='afficheBootbox2()'>&#9734</span>
	<span onclick='afficheBootbox2()'>&#9734</span>
	<span onclick='afficheBootbox2()'>&#9734</span>
	<span onclick='afficheBootbox2()'>&#9734</span>
	</div>
	</div><div class='col-md-9'> 
	";
                } else {
                    $this->contenu .= "
       <div class='col-md-3'> 
        <div class='stars '>
		<span onclick='vote5($_SESSION[idcompte],$_GET[id])' class='img-responsive stars'>&#9734</span>
		<span onclick='vote4($_SESSION[idcompte],$_GET[id])'class='img-responsive' stars>&#9734</span>
		<span onclick='vote3($_SESSION[idcompte],$_GET[id])'class='img-responsive' stars>&#9734</span>
		<span onclick='vote2($_SESSION[idcompte],$_GET[id])'class='img-responsive' stars>&#9734</span>
		<span onclick='vote1($_SESSION[idcompte],$_GET[id])'class='img-responsive' stars>&#9734</span>
</div>
</div><div class='col-md-9'> 
";

                }
            }
        }else{
            $this->contenu.="<div class='col-md-3'><div class='stars'>
	<span onclick='afficheBootbox(\"$token\")'>&#9734</span>
	<span onclick='afficheBootbox(\"$token\")'>&#9734</span>
	<span onclick='afficheBootbox(\"$token\")'>&#9734</span>
	<span onclick='afficheBootbox(\"$token\")'>&#9734</span>
	<span onclick='afficheBootbox(\"$token\")'>&#9734</span>
	</div>
	</div><div class='col-md-9'> ";
        }

        $this->contenu.="      <ul class=\"list-inline text-right img-responsive\">  <li>
                     <div id=\"fb-root\"></div>
                     
                        <div id=\"fb-root\"></div>
                            <script>(function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = \"//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.8\";
                              fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                        <div id='facebook' class=\"fb-share-button\" data-href=\"\" data-layout=\"button\" data-mobile-iframe=\"false\"><a class=\"fb-xfbml-parse-ignore\" target=\"_blank\" href=\"https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse\">Partager</a></div>
                        </li>
                        <li>
                            <a href=\"https://twitter.com/share\" id='twitter' class=\"twitter-share-button \" data-lang=\"fr\" data-size=\"small\" data-hashtags=\"shotpic\">Tweeter</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            
                        </li>
                          </ul>
                          </div>
                        </div>
                        </div>
                        </div>
                        <script src='module/mod_vote/vote.js'></script>    
                        ";
        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            if($_SESSION['admin']==0 && $_SESSION['modo']==0){
                $this->contenu.="<div  id=\"commenter\">
                        <form method='post' id='form' action='index.php?module=ajouterCommentaire' >
                        <input type='hidden' value=\" $_GET[id] \" name='idphoto'>
                         <input type='hidden' value=\" $token \" name='token'>
                        <div class=\"form-group\">
                            <h3 for=\"comment\" style='margin-left: 1% ; margin-top: 1%'>Commenter:</h3>
                           <div style='margin: 1%'>
                                <textarea class=\"form-control  commenter  \" placeholder='Ecrivez votre commentaire' rows=\"4\" id=\"comment\" name='contenu'></textarea>
                            </div>
                            <div class='text-right'>
                            <button type='submit' class=' btn  btn-primary' id='sub'  > commenter</button>
                            </div>
                         </div>
                        </form>
                        </div>                      
                                         
         ";
            }

        }
        $this->contenu.="<div id='commentaires' class=\"panel panel-default\">
                        <div class=\"panel-heading\">
						    		<h3 class=\"panel-title\">
                    Commentaires
                           </h3>
                         </div> ";
        foreach ($com as $c) {
            $c['pseudo']=htmlspecialchars($c['pseudo']);
            $c['contenu']=htmlspecialchars($c['contenu']);
            $this->contenu .= "                         
                            <div class=\"panel-body list-group-item-text com\"> 
                                    <p>$c[pseudo]</p><p>$c[dateCom]</p><p>$c[heureCom]</p><br>
                                    <p>$c[contenu]</p>";
            if ((isset($_SESSION) && isset($_SESSION['idcompte']) && isset($_SESSION['pseudo']) && isset($_SESSION['modo']) && isset($_SESSION['admin']))) {
                if ($_SESSION['admin'] == 1 || $_SESSION['modo'] == 1) {
                    $this->contenu .= "<button type=\"button\" class=\"btn btn-danger btn-circle btn-xl supr \" onclick=\"Affichesupprimer($c[idCommentaire])\">X</button> ";
                }
            }
            $this->contenu .= "
            </div>";
        }
        $this->contenu .= "</div>

    <script src='module/mod_details_photo/detail_photo.js'></script>
 ";  include_once ('include/footer.php');
    }
}
