xslw<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:52
 */
require_once('modele_details_photo.php');
require_once ('vue_details_photo.php');
class ControleurDetailPhoto extends ControleurGenerique
{


    /**
     * controleur_detail_photo constructor.
     */
    public function __construct()
    {
        $this->modele=new ModeleDetailsPhoto();
        $this->vue=new VueDetailsPhoto();
    }

    public function detail($idPhoto,$idcompte=0){
        $detail=$this->modele->getDetail($idPhoto);
        $com=$this->modele->getCom($idPhoto);
        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            $voter=$this->modele->getVoter();
        }else{
            $voter=array();
        }

        $token=$this->modele->createToken();

        $this->vue->vue_detail($detail[0],$com,$voter,$token);
	
    }
}
