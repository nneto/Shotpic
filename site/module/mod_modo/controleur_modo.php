<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:52
 */
require_once('modele_modo.php');
require_once('vue_modo.php');
class ControleurModo extends ControleurGenerique
{


    /**
     * controleur_detail_photo constructor.
     */
    public function __construct()
    {
        $this->modele=new ModeleModo();
        $this->vue=new VueModo();
    }

    public function afficherCompte(){
        $compte=$this->modele->getCompte();
        $this->vue->afficheCompte($compte);
    }

    public function addModo($tab){

    }

    public function delModo($tab){


    }
}
