<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:52
 */
class VueModo extends VueGenerique
{

    public function afficheCompte($compte)
    {

        $this->titre = "Compte";
        $this->Css = array("<link href='module/mod_modo/compte.css' rel='stylesheet'>");
        include("include/nav.php");
        $this->contenu.="  <div class=\"container\" id=\"central\">
                <div class='col-md-6' >
                <h1>Moderateur</h1>
                 <div class='col-md-12' id='modo'>
        ";
        foreach($compte as $c){
            $c['pseudo']=htmlspecialchars($c['pseudo']);
            if($c['modo']==1){
                $this->contenu.="<div class='col-md-3'>$c[pseudo]</div><div class='cold-md-3'><button type=\"button\" class=\"btn btn-danger btn-circle btn-xl\" onclick='delModo($c[idcompte])'>X</button></div> ";
            }
        }
        $this->contenu.=" 
                </div>
               </div>
               <div class='col-md-6' id='comtpte'>
                <h1>Compte</h1> 
                 <div class='col-md-12' id='compte'>
                ";

        foreach($compte as $c){
            $c['pseudo']=htmlspecialchars($c['pseudo']);
            if($c['modo']==0 && $c['admin']==0){
                        $this->contenu.= "<div id='bouton'><div class='col-md-12 compte'><div  class='col-md-3'>$c[pseudo]</div><div class='cold-md-3'><button type=\"button\" class=\"btn btn-success btn-circle btn-xl add\" onclick='addModo($c[idcompte])'>O</button><button type=\"button\" class=\"btn btn-danger btn-circle btn-xl\" onclick='Affichesupprimer($c[idcompte],\"$c[pseudo]\")'>X</button></div></div></div>" ;
            }
        }
        $this->contenu.="</div>
             </div>
             </div>
             <script src='module/mod_modo/compte.js'></script>
             ";  include_once ('include/footer.php');
    }
}
