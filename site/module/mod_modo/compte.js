/**
 * Created by NICO on 27/03/2017.
 */
function refreshModo(){
    $.ajax({
        url : 'module/mod_modo/refreshModo.php',
        type:'POST',

        dataType:'html',
        success : function(code_html){
            $("#modo").html(code_html);
        }});
}

function refreshCompte(){
    $.ajax({
        url : 'module/mod_modo/refreshCompte.php',
        type:'POST',

        dataType:'html',
        success : function(code_html){
            $("#compte").html(code_html);
        }});
}

function addModo(idcompte) {
    $.ajax({
        url : 'module/mod_modo/addModo.php',
        type:'POST',
        data: 'idcompte='+idcompte,
        dataType:'html',
        success : function(code_html){
          console.log(code_html);
          refreshModo();
          refreshCompte();
        }});
}

function delModo(idcompte,b){
    $.ajax({
        url : 'module/mod_modo/delModo.php',
        type:'POST',
        data: 'idcompte='+idcompte,
        dataType:'html',
        success : function(code_html){

            console.log(code_html);
            refreshModo();
            refreshCompte();
        }});
}

function delCompte(idcompte) {
    $.ajax({
        url : 'module/mod_modo/delCompte.php',
        type:'POST',
        data: 'idcompte='+idcompte,
        dataType:'html',
        success : function(code_html){
           $('.modal-open').modal("hide");
            refreshModo();
            refreshCompte();
        }});
}

function Affichesupprimer(idcompte,pseudo){
    var b =  bootbox.confirm({
        onEscape:true,
        backdrop :true,
        message : " voulez vous vraiment supprimer le compte de "+pseudo+" ? <br/>\
     	",
    callback:function(result) {
        if (result){
            delCompte(idcompte);
            $('#supprimer').submit();
    }else{

        }
    }
    });
    b.find('.modal-content').addClass("col-ms-8");

}
