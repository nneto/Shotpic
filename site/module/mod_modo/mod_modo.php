<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:47
 */
require_once('controleur_modo.php');
class ModModo extends ModuleGenerique
{

    /**
     * ModDetailPhoto constructor.
     */
    public function __construct()
    {
        $this->controleur= new ControleurModo();
    }

   public function afficherCompte(){
        $this->controleur->afficherCompte();
   }

   public function addModo($tab){
       $this->controleur->addModo($tab);
   }

   public function delModo($tab){
       $this->controleur->delModo($tab);
   }
}