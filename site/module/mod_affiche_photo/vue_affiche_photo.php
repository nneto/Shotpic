<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 16/02/2017
 * Time: 18:16
 */
class VueAffichePhoto extends VueGenerique
{
    public function afficher($photo,$token){
        $this->Css=array("<link href='module/mod_affiche_photo/affiche_photo.css' rel='stylesheet'>");
        $this->titre="photo";
        include ("include/nav.php");
        if(isset($_GET['err'])){
            $this->contenu.="<script>
                </script>
                ";
        }

        $this->contenu.="
        <div class=\"container\" id=\"central\">                    
            <div id='photos' class='text-center'>
            
        ";
        foreach ($photo as $p) {
            $this->contenu .= "<a href='index.php?module=detail&id=$p[idPhoto]'><div class='image ' style=\"background-image: url('source/$p[nomPhoto]')\" >";
            if ((isset($_SESSION) && isset($_SESSION['idcompte']) && isset($_SESSION['pseudo']) && isset($_SESSION['modo']) && isset($_SESSION['admin']))) {
                if ($_SESSION['admin'] == 1 || $_SESSION['modo'] == 1) {

                    $this->contenu .= "<button type=\"button\" class=\"btn btn-danger btn-circle btn-xl supr \" onclick=\"Affichesupprimer($p[idPhoto])\">X</button> ";
                }
            }else{
                $this->contenu .= "  </a>";
            }

            $this->contenu .= "  </div></a>";

        }
        $this->contenu.="
       </div>
        </div>
        <script src='module/mod_affiche_photo/affiche_photo.js' rel='text/javascript'></script>
        ";
        include_once ('include/footer.php');
    }

}