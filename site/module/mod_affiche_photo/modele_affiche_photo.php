<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 16/02/2017
 * Time: 18:15
 */
class  ModeleAffichePhoto extends ModeleGenerique
{
    public function getPhotos(){
        $requete= "select * from photo ORDER by datePhoto DESC ,heurePhoto DESC ";
        $requete=self::$connexion->prepare($requete);
        $requete->execute();
        return $requete->fetchall(PDO::FETCH_ASSOC);
    }


}