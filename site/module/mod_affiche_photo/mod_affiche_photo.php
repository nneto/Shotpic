<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 16/02/2017
 * Time: 18:15
 */
require_once ('controleur_affiche_photo.php');
class ModuleAffichePhoto extends ModuleGenerique
{


    /**
     * ModuleAffichePhoto constructor.
     */
    public function __construct()
    {
        $this->controleur= new ControleurAffichePhoto();
    }

    public function afficher(){
        $this->controleur->afficher();
    }
}