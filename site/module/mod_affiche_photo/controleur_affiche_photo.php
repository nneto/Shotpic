<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 16/02/2017
 * Time: 18:14
 */
require_once ('vue_affiche_photo.php');
require_once ('modele_affiche_photo.php');

class ControleurAffichePhoto extends ControleurGenerique
{


    /**
     * ControleurAffichePhoto constructor.
     */
    public function __construct()
    {
        $this->modele= new ModeleAffichePhoto();
        $this->vue= new VueAffichePhoto();
    }

    public function afficher(){
        $photo=$this->modele->getPhotos();
        $token=$this->modele->createToken(1200);
        shuffle($photo);
        $this->vue->afficher($photo,$token);

    }
}