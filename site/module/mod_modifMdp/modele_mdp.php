<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 14/03/2017
 * Time: 09:03
 */

require ('PHPMailer-master/PHPMailerAutoload.php');
class ModeleMdp extends ModeleGenerique
{

    public function setMdp($mdp,$email){
        $requeteEmail='update compte set motDePasse=? where idcompte=?';
        $requete=self::$connexion->prepare($requeteEmail);
        $requete->execute(array($mdp,$email));


    }

    public function getMdp($mdp){
        $requeteEmail='select * from compte where motDePasse=? and idcompte=? ';
        $requete=self::$connexion->prepare($requeteEmail);
        $requete->execute(array($mdp,$_SESSION['idcompte']));
        return $requete->fetch(PDO::FETCH_ASSOC);
    }


}