<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 14/03/2017
 * Time: 09:03
 */
require_once ("modele_mdp.php");
require_once ("vue_mdp.php");
class ControlleurMdp extends ControleurGenerique
{
    public function demande(){
        $this->modele=new ModeleMdp();
        $this->vue=new Vuemdp();
        $token=$this->modele->createToken();
        $this->vue->formMdp2($token,$_SESSION['idcompte']);
    }

    public function change($tab){
        $this->modele=new ModeleMdp();
        $o=$this->modele->getMdp($this->modele->mdpCrypt($tab['oldmdp'],""));
        $t=$this->modele->getToken($tab['token']);
        if(!empty($t)){
            if($tab['mdp']==$tab['mdp2']){
                if(empty($o)){
                    header('Location:index.php?module=accueil&err=ancien%20mdp%20different');
                }else{
                    $mdp=$this->modele->mdpCrypt($tab['mdp'],"");
                    $this->modele->setMdp($mdp,$_SESSION['idcompte']);
                    header('Location:index.php?module=accueil&err=mdp%20changer');
                }
            }else{

                header('Location:index.php?module=accueil&err=mdp%20different');
            }
        }else{
            header('Location:index.php?module=accueil&err=erreur%20token');
        }
    }

}