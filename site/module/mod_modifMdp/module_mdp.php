<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 14/03/2017
 * Time: 09:03
 */

require_once("controlleurMdp.php");
class ModuleMdp extends ModuleGenerique
{
    public function demande(){
        $this->controleur= new ControlleurMdp();
        $this->controleur->demande();
    }



    public function change($info,$idcompte){
        $this->controleur= new ControlleurMdp();
        $this->controleur->change($info,$idcompte);
    }
}