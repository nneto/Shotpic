<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 16/02/2017
 * Time: 18:15
 */
require_once('controlleur_vote.php');
class ModuleVote extends ModuleGenerique{


    /**
     * ModuleAffichePhoto constructor.
     */
    public function __construct()
    {
        $this->controleur= new ControleurVote();
    }

    public function afficher(){
        $this->controleur->ajoutVote();
    }
}
