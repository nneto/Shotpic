<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:52
 */
class VueProfil extends VueGenerique
{

    public function profil($photo, $com, $vote, $token,$info)
    {
        $this->titre = "profil";
        $this->Css = array("<link href='module/mod_profil/profil.css' rel='stylesheet'>");
        include("include/nav.php");
        $this->contenu .= "
        		<article >
				<div class='container'>
    					<div class='row profile '>
						<div class='col-md-3 thumbnail' id='profil'>
							<div class='profile-sidebar'>
			
								<div class='profile-userpic'>";
        if ($_SESSION['modo']==1 || $_SESSION['admin']==1) {
        }else{

            if (empty($photo)) {
                $this->contenu .= "<h2>Photo poster :</h2>	<p   > Aucune photo poster</p>";
            } else{
                $this->contenu .= "	<h2>Photo poster :</h2> <img src='source/$photo[nomPhoto] '  class='img-responsive' alt='Aucune photo poster'>";
            }
        }
        $this->contenu .="
								</div>
								<div class='profile-usertitle'>
									<div class='profile-usertitle-name'>
										$info[nom] $info[prenom]
									</div>
								</div>												
								<div class='profile-userbuttons '>
								<button type='button' style='width: 50% ; font-size: 9px' onclick='location.href=\"index.php?module=modifMdp\"' class='btn btn-success btn-sm'>Changer mot de passe</button>								
								</div>					
								<div class='profile-usermenu'>
									<ul class='nav'>
										<li>		<a href='index.php?module=modifEmail' target='_blank'>								
												<i class='glyphicon glyphicon-user'></i>
											$info[pseudo] </a>
										</li>
								
										<li>
											<a href='index.php?module=modifEmail' target='_blank'>
												<i class='glyphicon glyphicon-envelope'></i>
											$info[email] </a>
										</li>
										<li>
											
										</li>
									</ul>
								</div>
				
							</div>
						</div>
						<div class='col-md-8 ' id='main'>
							<ul class='nav nav-pills'>
    							<li class='active'><a data-toggle='pill' href='#votes'>Mes votes</a></li>
   					 		
   					 		<li><a data-toggle='pill' href='#com' id='b' onclick='affiche2($_SESSION[idcompte])' >Mes Commentaire</a></li>
   					 	</ul>   					 	   					 	    								
            						<div class='profile-content thumbnail'>
            						<div class='tab-content'>
            							<div id='votes' class='tab-pane fade in active ' style=\"overflow-y: scroll; height:500px;\">
            			                           ";
        foreach($vote as $v){
            $this->contenu .=" <div class='col-md-12'> <img src='source/$v[nomPhoto]' id='' class='img-responsive col-md-5' /><div class='col-md-7'><div class='col-md-1'> <p class='note'>$v[note]</p></div><div class='stars col-md-2'> <span  >&#9734</span> </div> </div></div>";

        }
        $this->contenu .="	        
								</div>
								<div id='com' class='tab-pane fade' style=\"overflow-y: scroll; height:500px;\">
									
								";
        foreach($com as $c){
            $this->contenu .=" <div class='col-md-12'> <img src='source/$c[nomPhoto]' id='' class='img-responsive col-md-5' /><div class='col-md-7'><div>$c[dateCom] $c[heureCom]</div><div> <p class='note'>$c[contenu]</p></div></div></div>";

        }
        $this->contenu .="		</div>				</div>
						</div>
					</div>
				</div>
				<br>
				<br>
			
				<script src='module\mod_profil\profil.js'></script>
				
		</article>
        ";  include_once ('include/footer.php');
    }
}
