<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:53
 */
class ModeleProfil extends ModeleGenerique
{
    public function getInfo(){
        $requete="select * from  compte where idCompte=? LIMIT 1";
        $requete=self::$connexion->prepare($requete);
        $requete->execute(array($_SESSION['idcompte']));
        return $requete->FetchAll(PDO::FETCH_ASSOC);
    }

    public function getCom(){
        $requete="select commentaire.contenu , commentaire.dateCom,commentaire.heureCom,photo.nomPhoto,photo.idPhoto from commentaire inner join photo on commentaire.idPhoto=photo.idPhoto where commentaire.idCompte=?";
        $requete=self::$connexion->prepare($requete);
        $requete->execute(array($_SESSION['idcompte']));
        return $requete->FetchAll(PDO::FETCH_ASSOC);
    }

    public function getPhoto(){
        $requete="select * from photo natural join compte where idCompte=? LIMIT 1";
        $requete=self::$connexion->prepare($requete);
        $requete->execute(array($_SESSION['idcompte']));
        return $requete->FetchAll(PDO::FETCH_ASSOC);
    }

    public function getVote(){
        $requete="select voter.note , voter.idPhoto ,voter.idcompte,photo.nomPhoto,photo.titrePhoto from voter inner join photo on voter.idPhoto=photo.idPhoto where voter.idcompte=? ";
        $requete=self::$connexion->prepare($requete);
        $requete->execute(array($_SESSION['idcompte']));
        return $requete->FetchAll(PDO::FETCH_ASSOC);
    }

    public function setEmail($email){

    }

    public function setMdp($mdp){

    }
}
