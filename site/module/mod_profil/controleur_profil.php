<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:52
 */
require_once('modele_profil.php');
require_once('vue_profil.php');
class ControleurProfil extends ControleurGenerique
{


    /**
     * controleur_detail_photo constructor.
     */
    public function __construct()
    {
        $this->modele=new ModeleProfil();
        $this->vue=new VueProfil();
    }

    public function afficheProfil(){
        $photo=$this->modele->getPhoto();

        $vote=$this->modele->getVote();
        $com=$this->modele->getCom();
        $info=$this->modele->getInfo();
        $token=$this->modele->createToken();
        $this->vue->profil($photo[0], $com, $vote, $token,$info[0]);
    }

    public function setMdp($tab){

    }

    public function setEmail($tab){


    }
}
