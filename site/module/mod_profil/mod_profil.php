<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 22/02/2017
 * Time: 23:47
 */
require_once('controleur_profil.php');
class ModProfil extends ModuleGenerique
{

    /**
     * ModDetailPhoto constructor.
     */
    public function __construct()
    {
        $this->controleur= new ControleurProfil();
    }

   public function afficherProfil(){
        $this->controleur->afficheProfil();
   }

   public function setMdp($tab){
       $this->controleur->setMdp($tab);
   }

   public function setEmail($tab){
       $this->controleur->setEmail($tab);
   }
}