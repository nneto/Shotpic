<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 14/03/2017
 * Time: 01:47
 */
require_once ("vue_cgu.php");
    class ControleurCgu extends ControleurGenerique
{
    public function afficher(){
        $this->vue=new VueCgu();
        $this->modele=new ModeleGenerique();
        $t=$this->modele->createToken();
        $this->vue->afficher($t);
    }

}