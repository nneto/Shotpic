<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 14/03/2017
 * Time: 01:45
 */
class VueCgu extends VueGenerique
{

    public function afficher($token){
        include("include/nav.php");
        $this->Css=array("<link href='module/mod_cgu/Cgu.css' rel='stylesheet'>");
        $this->titre="CGU";
        $this->contenu.="
          <div class=\"container\" id=\"central\">
                <p>
                
                <h1>Présentation de Shotpic</h1>
                <p>Shotpic est un site créé par trois étudiants de l’IUT de Montreuil dans le cadre de leur projet universitaire. Aidés par des étudiantes de communication, Shotpic a organisé un concours photo autour du portrait intitulé « Paris 8 en portraits ». Toutes les personnes intéressées sont invitées à participer et à concourir pour le prix du meilleur portrait. </p>
                <h1>Règlement </h1>
                <p>Pour participer au concours photo, rien de plus simple : <br/>
                    1. Réalise ton portrait<br/>
                    2. Rends-toi à partir du 20 Avril sur le site Shotpic<br/>
                    3. Inscris-toi et poste ton cliché accompagné d’un descriptif<br/> 
                    4. Vote pour ta photo préférée à partir du 6 juin<br/>
                    5. Rends-toi à l’exposition photo organisée le 12 Juin à l’IUT de Montreuil pour connaître le grand vainqueur de l’exposition<br/> 
                Tu recevras toutes les informations sur la remise des prix dans ta boite mail.<br/>
                Pour en savoir plus, appelle au 06.02.06.76.40 et retrouve Shotpic sur Facebook et Instagram (Shotpic_concours).<br/>    
                </p>
                <h1>Conditions pour l'autorisation d'exposition:    </h1>
                <p>Je consens à donner tous les droits à « Paris 8 en portraits » que ce soit d’exposition ou de reproduction. 
                  J’accepte notamment que ma photographie soit exposée lors de l’exposition « Paris 8 en portraits » en juin 2017 et qu’elle soit utilisée à des fins promotionnelles dans un territoire illimité.
                  J’autorise également l’université à conserver ces clichés dans la bibliothèque numérique ainsi qu’en format papier dans les archives de celle-ci. 
                  Je consens aussi à ne percevoir aucune rémunération que ce soit en argent ou en nature et à n’engager aucune poursuite judiciaire.
                  La présente autorisation est consentie sans limitation de durée. Ces dispositions sont portées à notre connaissance, dans le cadre de l’application de la législation relative au respect du droit à l’image et au respect de la vie privée.
                  En acceptant ledit contrat, je certifie que toute personne présente sur la photographie soit consentante et qu’elle accepte elle-aussi les termes du contrat.</p>
                <h1>A propos du portrait</h1>
                <p>Le portrait tient une place importante dans le monde de l'art.
Et si le portrait est présent tout au long de son histoire, il est tout aussi présent dans la pratique artistique de beaucoup d’artistes contemporains ainsi que dans la culture populaire (portraits de rue, bandes dessinées, graffitis, caricatures, etc.). De plus, notre univers quotidien est envahi par une multitude d’images de visages : portraits stéréotypés et idéalisés issus du domaine de la mode, de la publicité et images diverses originaires de la culture de masse (vidéos clip, pochettes de disque, sites web, etc.).

L'origine du terme « portrait » remonte au XIIe siècle. Les rois, les princes, les seigneurs, les puissants en quête d'immortalité, souhaitaient qu'au moins leurs traits passent à la postérité. Comment? En s'adressant aux peintres : faites un tableau pour immortaliser mes traits. Le tableau pour les traits est devenu peu à peu un pour-trait et plus tard un portrait. 

Cette élite sociale et culturelle confiait donc à des artistes le soin de réaliser une effigie glorieuse de leur personne en prenant soin de les représenter au côté d'objets symbolisant leur ascension sociale.
Cette opération ne se déroulait pas sans entorse au référent car ces représentations s'accomplissaient souvent au prix de trucages, d'idéalisations dont les portraitistes se faisaient les complices.

La photographie a ensuite modifié le rôle et l’usage du portrait.
En plus d'avoir modifié le rôle de l'artiste et sa manière de concevoir un portrait, l'apparition de la photographie a permis aussi de démocratiser ce qui, jusqu'alors, demeurait l'apanage des privilégiés ou des puissants. Se voir soi-même (autrement que dans un miroir) est un acte récent, le portrait, peint, dessiné ou miniaturisé, ayant été jusqu'à la diffusion de la photographie un bien restreint, destiné d'ailleurs à afficher un standing financier et social.
Pour autant, indépendamment du statut d’un individu, la photo ne capte pas réellement l’identité du sujet. L’identité du sujet étant mouvante, sa représentation ne peut être donnée de manière objective.
Amplifier, exagérer, déformer, ce désir d'altérité  ou de subversion ont été des données explorées par certains artistes contemporains comme par exemple : Mapplethorpe, Cumming, Wearing, Appelt et Cindy Sherman, laquelle remet en question l'image de son propre visage et par le fait même, l'image stéréotypée de la femme véhiculée par les médias.
Beaucoup d’artistes s’interrogent également sur la négation de l’identité à laquelle peut conduire le portrait. Il est important de le rappeler, au XIXe siècle, les spécialistes de la physiognomonie médicale croyaient détenir un moyen scientifique pour lire le visage. Cette tentative s'est naturellement soldée par un échec.
L'image retrouve curieusement, à travers l'illusion « scientiste », sa fonction magique de dévoilement. Il ne faut pas oublier les tentatives de classifications raciales à travers le portrait photographique (nazisme) qui marquent bien les limites de l'entreprise pseudo-scientifique, inscrite dans les institutions policières depuis Bertillon, fondateur du fichage anthropométrique.

La révolution féministe a, à sa façon, réhabilité le portrait. En nous permettant de réviser notre manière de concevoir l'histoire de l’art et de mettre en évidence la relégation des femmes aux arts décoratifs (ou arts appliqués) au cours des siècles. Beaucoup de  femmes s'adonnaient à la peinture - de portraits, de fleurs, de nature morte, etc… réalisations plus modestes que les grandes murales ou peintures d’envergures historiques, religieuses. Le portrait pouvait donc être connoté de « féminin», ou du moins considéré comme un champ mineur de l'art. Nombreuses sont les femmes artistes qui abordent justement des sujets directement liés aux rôles traditionnels de la femme pour mieux les subvertir.
Le portrait se voit  donc propulsé dans l’univers d’une identité en mouvance, permutable, nomade, surprenante et qui, sous le regard d’un ou d’une artiste, peut devenir étrange ou non familier. 

Le portrait serait donc un « Je » signifiant, non pas parce qu’il possède une existence profonde et stable, mais parce qu’il est porteur de connexions signifiantes et insolites, qu’il construit son sens avec les autres et en regard des liens qu’il tisse avec son environnement.</p>
                </div>

            
        
        
        
        
        
        ";
        include_once ('include/footer.php');
    }
}