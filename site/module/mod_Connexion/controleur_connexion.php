<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 09/11/16
 * Time: 18:42
 */
require_once('model_connexion.php');
require_once('vue_connexion.php');
class ControleurConnexion extends ControleurGenerique
{
    public function __construct()
    {
        $this->vue=new VueConnexion();
        $this->modele=new ModelConnexion();
    }

    public function controleur_inscription($tab){
        $r=$this->modele->getToken($tab['token']);
        if(empty($r)){
            $this->modele->effacerToken($tab['token']);
            header('Location:index.php?module=accueil&err=erreur%20token');
        }else {
            $this->modele->effacerToken($tab['token']);
            $compte = $this->modele->getCompte($tab);

            if ($compte == NULL) {
                $this->modele->effacerToken($tab['token']);
                header('Location:index.php?module=accueil&err=identifiant%20incorrect');
            }else if($compte[0]['activer']=="0"){
                $this->modele->effacerToken($tab['token']);
                header('Location:index.php?module=accueil&err=compte%20non%20activer%20checkez%20vos%20mail');
            }else{
                $this->modele->effacerToken($tab['token']);
                $_SESSION['pseudo']=$compte[0]['pseudo'];
                $_SESSION['idcompte']=$compte[0]['idcompte'];
                $_SESSION['modo']=$compte[0]['modo'];
                $_SESSION['admin']=$compte[0]['admin'];
                header('Location:index.php?module=accueil&err=connexion%20effectuer');
            }
        }

    }
}