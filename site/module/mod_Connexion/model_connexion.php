<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 09/11/16
 * Time: 18:44
 */
class ModelConnexion extends ModeleGenerique
{
    public function getCompte($tab){
        try{
            $requeteCompte='select * from compte where pseudo=? and motDePasse=?';
            $pseudo=$tab['pseudo'];
            $mdp=$this->mdpCrypt($tab['mdp'],$pseudo);
            $requete=self::$connexion->prepare($requeteCompte);
            $requete->execute(array($pseudo,$mdp));
            $tab2=$requete->FetchAll(PDO::FETCH_ASSOC);


            return $tab2;
        }catch (PDOException $p){
            throw new ModeleInscriptionException();
        }
    }

}