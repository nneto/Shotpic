<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 14/03/2017
 * Time: 09:03
 */

require_once("controlleurMdp.php");
class ModuleMdp extends ModuleGenerique
{
    public function demande(){
        $this->controleur= new ControlleurMdp();
        $this->controleur->demande();
    }

    public function formMdp2($token,$idcompte){
        $this->controleur= new ControlleurMdp();
        $this->controleur->formMdp2($token,$idcompte);
    }

    public function envoi($info){
        $this->controleur= new ControlleurMdp();
        $this->controleur->envoi($info);
    }

    public function change($info,$idcompte){
        $this->controleur= new ControlleurMdp();
        $this->controleur->change($info,$idcompte);
    }
}