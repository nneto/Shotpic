<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 14/03/2017
 * Time: 09:05
 */
class Vuemdp extends VueGenerique
{
    public function formMdp($token){
        $this->titre="Change de mot de passe";
        $this->Css=array("<link href='module/mod_mdpOublier/mdp.css' rel='stylesheet'>");
        include("include/nav.php");
        $this->contenu.=" 
            <div class=\"container\" id=\"central\">
                <div class='col-md-12' >
                <p> Entrez votre email pour changer de mot de passe </p>
                </div>
                <form action='index.php?module=mdp' method='post'>
                <input type='hidden' name='token' value='$token'>
                <div class='form-group'>
                    <input type='email' name='email' required>
                </div>
                    <input type='submit' id='b' class='btn btn-action btn-primary'>
                </form>
                
            
            </div>
        ";
    }

    public function formMdp2($token,$idcompte)
    {
        $this->titre = "Change de mot de passe";
        $this->Css = array("<link href='module/mod_mdpOublier/mdp.css' rel='stylesheet'>");
        include("include/nav.php");
        $this->contenu .= " 
            <div class=\"container\" id=\"central\">
                <div class='col-md-12' >
                <p> Entrez votre email pour changer de mot de passe </p>
                </div>
                <form action='index.php?module=mdp' method='post'>
                <input type='hidden' name='token' value='$token'>
                 <input type='hidden' name='idcompte' value='$idcompte'>
            <div class='form-group'>
                  <label class='control-label col-sm-3' for='mdp'>mot de passe</label>
               <div class='col-sm-8'>
                   <input class='form-control' type='password' name='mdp' id='mdp' required>
               </div>
           </div>
           <div class='form-group'>
                  <label class='control-label col-sm-3' for='mdp2'>confirmer le mot de passe</label>
               <div class='col-sm-8'>
                   <input class='form-control' type='password' name='mdp2' id='mdp2s' required>
               </div>
           </div>
                    <input type='submit' id='b' class='btn btn-action btn-primary'>
                </form>                          
            </div>
        ";
    }




}