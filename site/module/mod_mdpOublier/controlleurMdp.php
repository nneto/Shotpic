<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 14/03/2017
 * Time: 09:03
 */
require_once ("modele_mdp.php");
require_once ("vue_mdp.php");
class ControlleurMdp extends ControleurGenerique
{
    public function demande(){
        $this->modele=new ModeleMdp();
        $this->vue=new Vuemdp();
        $token=$this->modele->createToken();
        $this->vue->formMdp($token);
    }
    public function formMdp2($token,$idcompte){
        $this->modele=new ModeleMdp();
        $this->vue=new Vuemdp();
        $this->vue->formMdp2($token,$idcompte);
    }
    public function envoi($tab){
        $this->modele=new ModeleMdp();
        $this->vue=new Vuemdp();
        $r=$this->modele->getToken($tab['token']);
        if(empty($r)){
            $this->modele->effacerToken($tab['token']);
            header('Location:index.php?module=accueil&err=erreur%20token');
        }else {
            $this->modele->effacerToken($tab['token']);
            $email=$this->modele->getEmail($tab['email']);

            if( !empty($email)){
                $this->modele->sendMail($tab['email'],$email[0]['idcompte']);
                session_destroy();
                header('Location:index.php?module=accueil&err=email%20envoyé');
            }else{
                header('Location:index.php?module=accueil&err=email%20invalide');
            }
        }
    }
    public function change($tab,$idcompte){
        var_dump($tab);
        $this->modele=new ModeleMdp();
        if($tab['mdp']==$tab['mdp2']){
            $mdp=$this->modele->mdpCrypt($tab['mdp'],"");
            $this->modele->setMdp($mdp,$tab['idcompte']);
            header('Location:index.php?module=accueil&err=mdp%20changer');
        }else{

            header('Location:index.php?module=accueil&err=mdp%20different');
        }

    }

}