<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 15/11/16
 * Time: 16:22
 */
class VueAccueil extends VueGenerique
{
    public function vue_Accueil($token)
    {
        $this->titre = "Concours Photo";
        $this->Css = array("<link href=\"module/mod_accueil/depotTest.css\" rel=\"stylesheet\">");
        $this->contenu = " 	
          <img class=\" center-block img-responsive\" id=\"logo\" src=\"titre2.png\">
         <ul id=\"liste\" class=\"list-inline text-center\">
        <li>
            <a class=\"btn btn-index btn-responsive btn-sm\" href=\"index.php?module=CGU\">Règlement</a>
        </li>";
        if ((isset($_SESSION) && isset($_SESSION['idcompte']) && isset($_SESSION['pseudo']) && isset($_SESSION['modo']) && isset($_SESSION['admin']) )){
            if($_SESSION['admin']==1){
                $this->contenu .= " <li>
            <a class=\"btn btn-index btn-responsive btn-sm\" href='index.php?module=classement'>Classement</a>
        </li> "	;
            }elseif($_SESSION['modo']==1){

            }else{
                $this->contenu .= " <li>
            <a class=\"btn btn-index btn-responsive btn-sm\" onclick='afficheBootboxDepotPhoto(\"$token\")'>Participer</a>
        </li> "	;
            }

        }else{
            $this->contenu .= "  <li>
            <a class=\"btn btn-index btn-responsive btn-sm\" onclick='afficheBootbox(\"$token\")'>Participer</a>
        </li> 	";
        }
        $this->contenu .= "
        <li>
            <a class=\"btn btn-index btn-responsive btn-sm\" href=\"index.php?module=affiche\">Photos</a>
        </li>
        <li>";
        if ((isset($_SESSION) && isset($_SESSION['idcompte']) && isset($_SESSION['pseudo']) && isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            if($_SESSION['admin']==1){
                $this->contenu .= "
                <a class=\"btn btn-index btn-responsive btn-sm\" href=\"index.php?module=profil\">Modérateur</a>
       </li>
        <li>
           <a class=\"btn btn-index btn-responsive btn-sm\" href=\"index.php?module=deconnexion\" >Déconnexion</a>
        </li>       
    </ul>    
    ";
            }else{
                $this->contenu .= "
                <a class=\"btn btn-index btn-responsive btn-sm\" href=\"index.php?module=profil\">Profil</a>
       </li>
        <li>
           <a class=\"btn btn-index btn-responsive btn-sm\" href=\"index.php?module=deconnexion\" >Déconnexion</a>
        </li>       
    </ul>    
    ";
            }

        } else {
            $this->contenu .= "
           <a class=\"btn btn-index btn-responsive btn-sm\" href=\"#\" onclick='afficheBootbox( \"$token\")'>Se connecter / S'inscrire</a>
        </li>      
    </ul>   
    ";
        }
        include_once ('include/footer.php');
    }

    public function vue_err_Accueil($token,$err)
    {
        $this->titre = "Concours Photo";
        $this->Css = array("<link href=\"module/mod_accueil/depotTest.css\" rel=\"stylesheet\">");
        $this->contenu = " 
 	<script>$(document).ready(function() {
 	  var b = bootbox.dialog({
            size :'medium',
            onEscape: true,
            backdrop:true,
            message:   \" <div>\
        <div id = 'form'> \
            <h1> $err</h1> \
                </div> \
        <div> \"
        }
        , function(result) {
            if(result)
                $('#infos').submit();
        })
     b.find('.modal-content').addClass(\"col - ms - 8\");
 	})     
    </script>
     <img class=\" center-block img-responsive\" id=\"logo\" src=\"titre2.png\">
         <ul id=\"liste\" class=\"list-inline text-center\">
        <li>
            <a class=\"btn btn-index btn-responsive btn-sm\" href=\"index.php?module=CGU\">Règlement</a>
        </li>";
        if ((isset($_SESSION) && isset($_SESSION['idcompte']) && isset($_SESSION['pseudo']) && isset($_SESSION['modo']) && isset($_SESSION['admin']))) {
            if ($_SESSION['admin'] == 1) {
                $this->contenu .= " <li>
            <a class=\"btn btn-index btn-responsive btn-sm\" href='index.php?module=classement'>Classement</a>
        </li> ";
            } elseif ($_SESSION['modo'] == 1) {

            } else {
                $this->contenu .= " <li>
            <a class=\"btn btn-index btn-responsive btn-sm\" onclick='afficheBootboxDepotPhoto(\"$token\")'>Participer</a>
        </li> ";
            }

        } else {
            $this->contenu .= "  <li>
            <a class=\"btn btn-index btn-responsive btn-sm\" onclick='afficheBootbox(\"$token\")'>Participer</a>
        </li> 	";
        }
        $this->contenu .= "
        <li>
            <a class=\"btn btn-index btn-responsive btn-sm\" href=\"index.php?module=affiche\">Photos</a>
        </li>
        <li>";
        if ((isset($_SESSION) && isset($_SESSION['idcompte']) && isset($_SESSION['pseudo']) && isset($_SESSION['modo']) && isset($_SESSION['admin']))) {
            if ($_SESSION['admin'] == 1) {
                $this->contenu .= "
                <a class=\"btn btn-index btn-responsive btn-sm\" href=\"index.php?module=modo\">Modérateur</a>
       </li>
        <li>
           <a class=\"btn btn-index btn-responsive btn-sm\" href=\"index.php?module=deconnexion\" >Déconnexion</a>
        </li>       
    </ul>    
    ";
            } else {
                $this->contenu .= "
                <a class=\"btn btn-index btn-responsive btn-sm\" href=\"index.php?module=profil\">Profil</a>
       </li>
        <li>
           <a class=\"btn btn-index btn-responsive btn-sm\" href=\"index.php?module=deconnexion\" >Déconnexion</a>
        </li>       
    </ul>    
    ";
            }

        } else {
            $this->contenu .= "
           <a class=\"btn btn-index btn-responsive btn-sm\" href=\"#\" onclick='afficheBootbox( \"$token\")'>Se connecter / S'inscrire</a>
        </li>      
    </ul>   
    ";
        }
        include_once ('include/footer.php');
    }
}