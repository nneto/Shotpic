<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 15/11/16
 * Time: 16:15
 */

require_once('vue_accueil.php');
require_once ('modele_acueil.php');
class ControleurAccueil extends ControleurGenerique
{

    public function __construct()
    {
        $this->vue=new VueAccueil();
        $this->modele=new ModeleAcueil();
    }

    public function accueil(){

       $token= $this->modele->createToken();
        $this->vue->vue_Accueil($token);
    }

    public function accueil_err($err){
        $token= $this->modele->createToken();
        $this->vue->vue_err_Accueil($token,$err);
    }
}