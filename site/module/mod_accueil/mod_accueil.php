<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 15/11/16
 * Time: 16:09
 */

include_once('controleur_accueil.php');
class ModAccueil extends ModuleGenerique
{
    public function module_accueil(){
        $this->controleur= new ControleurAccueil();
        $this->controleur->accueil();
    }

    public function module_err_accueil($err){
        $this->controleur= new ControleurAccueil();
        $this->controleur->accueil_err($err);
    }

}