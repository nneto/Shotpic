<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 14/03/2017
 * Time: 09:05
 */
class VueEmail extends VueGenerique
{
    public function formEmail($token){
        $this->titre="Change de mot de passe";
        $this->Css=array("<link href='module/mod_mdpOublier/mdp.css' rel='stylesheet'>");
        include("include/nav.php");
        $this->contenu.=" 
            <div class=\"container\" id=\"central\">
                <div class='col-md-12' >
                <p> Entrez votre email pour changer de mot de passe </p>
                </div>
                <form action='index.php?module=modifEmail' method='post'>
                <input type='hidden' name='token' value='$token'>
                <div class='form-group'>
                    <input type='email' name='email' required>
                </div>
                    <input type='submit' id='b' class='btn btn-action btn-primary' value='Changer'>
                </form>            
            </div>
        ";  include_once ('include/footer.php');
    }



}