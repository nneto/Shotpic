<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 14/03/2017
 * Time: 09:03
 */
require_once("modele_ModifEmail.php");
require_once("vue_ModifEmail.php");
class ControlleurEmail extends ControleurGenerique
{
    public function demande(){
        $this->modele=new ModeleEmail();
        $this->vue=new VueEmail();
        $token=$this->modele->createToken();
        $this->vue->formEmail($token);
    }
    public function change($tab){
        var_dump($tab);
        $this->modele=new ModeleEmail();
        $t=$this->modele->getToken($tab['token']);
        if(!empty($t)){
            $this->modele->setEmail($tab['email']);
            header('Location:index.php?module=accueil&err=email%20changer');
        }else{
            header('Location:index.php?module=accueil&err=erreur%20token');
        }
}

}