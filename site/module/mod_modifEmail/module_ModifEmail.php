<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 14/03/2017
 * Time: 09:03
 */

require_once("controlleurModifEmail.php");
class ModuleEmail extends ModuleGenerique
{
    public function demande(){
        $this->controleur= new ControlleurEmail();
        $this->controleur->demande();
    }



    public function change($tab){

        $this->controleur= new ControlleurEmail();
        $this->controleur->change($tab);
    }
}