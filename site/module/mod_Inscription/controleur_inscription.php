<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 02/11/2016
 * Time: 21:02
 */
require_once('model_inscription.php');
require_once('vue_inscription.php');
class ControleurInscription extends ControleurGenerique
{

    /**
     * ControleurInscription constructor.
     */
    public function __construct()
    {
        $this->vue=new VueInscription();
        $this->modele=new ModelInsctription();
    }


    public function controleur_inscription($tab)
    {

        $r=$this->modele->getToken($tab['token']);
        if(empty($r)){
            $this->modele->effacerToken($tab['token']);
            header('Location:index.php?module=accueil');
        }else{
            $this->modele->effacerToken($tab['token']);
            $correct = $this->modele->addCompte($tab);
            if ($correct == "email fail") {
                header('Location:index.php?module=accueil&err=email%20deja%20utilisé');
            } else if ($correct == "pseudo fail") {
                header('Location:index.php?module=accueil&err=pseudo%20deja%20utilisé');
            }else if($correct=="mdp different"){
                header('Location:index.php?module=accueil&err=erreur%20inscription');
            }else{
                $this->modele->sendMail($tab['email'],$correct);
                header('Location:index.php?module=accueil&err=inscription%20effectuée%20Checker%20vos%20Mail');
            }
        }
    }
}