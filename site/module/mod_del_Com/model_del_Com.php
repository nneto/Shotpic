<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 09/11/16
 * Time: 18:44
 */
class ModelDelCom extends ModeleGenerique
{
    public function delCom($id){
        try{
            $requeteCompte='delete from commentaire where idCommentaire=?';
            $requete=self::$connexion->prepare($requeteCompte);
            $requete->execute(array($id));
        }catch (PDOException $p){
            throw new ModeleInscriptionException();
        }
    }

}