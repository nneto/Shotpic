<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 09/11/16
 * Time: 18:42
 */
require_once('model_del_Com.php');

class ControleurDelCom extends ControleurGenerique
{
    public function __construct()
    {
        $this->modele=new ModelDelCom();
    }

    public function delCom($id){
        $this->modele->delCom($id);
        header('Location:index.php?module=affiche');
    }
}