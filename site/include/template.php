<!DOCTYPE>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> <?php echo $module->getControleur()->getVue()->getTitre();?></title>
    <script src="jquery-3.1.1.min.js"></script>
    <link href="bootstrap-theme.min.css" rel="stylesheet">
    <link href="bootstrap.min.css" rel="stylesheet">
    <script src="include/test.js"></script>
    <script src="bootstrap.min.js"></script>
    <script src='bootbox.min.js' type='text/javascript'></script>
    <script src="include/test.js" type='text/javascript'></script>
    <!-- Optional theme -->


    <?php
    $css=$module->getControleur()->getVue()->getCss();
    if(empty($css)){
    }else{
        foreach ($css as $cs){
            echo $cs.
                "\n";
        }
    }
    ?>
</head>
<body>

<?php

echo $module->getControleur()->getVue()->getContenu();
?>
</body>


</html>