<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 12/10/16
 * Time: 09:52
 */
class VueGenerique
{
    protected $contenu;
    protected $titre;
    protected $Css;

    /**
     * VueGenerique constructor.
     * @param $contenu
     * @param $titre
     */
    public function __construct(){
        $this->contenu ="";
        $this->titre ="";
        $this->Css ="";
        ob_start();
    }

    public function  vue_tamponVersContenu(){
        $this->contenu.=ob_get_clean();

    }

    public function vue_erreur($erreur){
        $this->titre ="error";
      
       $this->contenu= "<p style='color:red;'>$erreur</p>";
    }

    public function vue_confirm($erreur){
        echo "<FONT><FONT color=green >$erreur </FONT></p>";
    }

    /**
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    public function getCss()
    {
        return $this->Css;
    }


}