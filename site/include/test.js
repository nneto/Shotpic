var nomCorrect=false;
var prenomCorrect=false;
var pseudoCorrect=false;
var motDePasseCorrect=false;
var emailCorrect=false ;



var password = document.getElementById("mdp");
var confirm_password = document.getElementById("mdp2");

function validatePassword(){
    if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
    } else {
        confirm_password.setCustomValidity('');
    }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;




function afficheBootbox(token) {
    var b = bootbox.dialog({
            size :'medium',
            onEscape: true,
            backdrop:true,
            className:"",
            message:   " <div  >\
                <ul class='nav nav-pills'>\
                    <li data-toggle='pill' class='active' onclick='connexion(\""+token+"\")'> <a>Connexion</a> </li>\
                    <li data-toggle='pill' onclick='inscription(\""+token+"\")' > <a>Inscription</a> </li>\
                </ul>\
                <div id='form' >\
                    <form style='margin-top: 1%'  class='form-horizontal' action='index.php?module=connexion' method='post'>\
                    <input type='hidden' name='token' value='"+token+"'>\
                    <div class='form-group'>\
                        <label class='control-label col-sm-3 ' for='pseudo'> Pseudo</label>\
                        <div class='col-sm-8'>\
                            <input style='width: 100%' class='form-control ' type='text' id='pseudo'  name='pseudo'  required> </div>\
                        </div>\
                        <div class='form-group'>\
                            <label for='mdp' class='control-label col-sm-3'>Mot de passe</label> \
                            <div class='col-sm-8'>\
                                <input class='form-control' type='password' name='mdp' id='mdp' required><br/>\
                                <a href='index.php?module=mdp'> Mot de passe oublier </a>\
                            </div>\
                        </div>\
                        <input type='submit' class='btn btn-primary btn-responsive' value='se connecter'>\
                 </div>\
                  </form>\
                </div>\
             </div> \ "
        }
        , function(result) {
            if(result)
                $('#infos').submit();
        })
    b.find('.modal-content').addClass("col-ms-8");
}

function connexion(token) {
    $('#form').html(" \
                <form style='margin-top: 1%'  class='form-horizontal' action='index.php?module=connexion' method='post'>\
                    <input type='hidden' name='token' value='"+token+"'>\
                    <div class='form-group'>\
                        <label class='control-label col-sm-3 ' for='pseudo'> Pseudo</label>\
                        <div class='col-sm-8'>\
                            <input style='width: 100%' class='form-control ' type='text' id='pseudo'  name='pseudo' required> </div>\
                        </div>\
                        <div class='form-group'>\
                            <label for='mdp' class='control-label col-sm-3'>Mot de passe</label> \
                            <div class='col-sm-8'>\
                                <input class='form-control' type='password' name='mdp' id='mdp' required>\
                                <a href='index.php?module=mdp'> Mot de passe oublier </a>\
                            </div>\
                        </div>\
                        <input type='submit' class='btn btn-primary btn-responsive' value='se connecter'>\
                 </div>\
                  </form>\
                  ");
}

function inscription(token) {
    $('#form').html(" \
     <form style='margin-top: 1%' class='form-horizontal' action='index.php?module=inscription' method='post'>\
        <input type='hidden' name='token' value='"+token+"'>\
           <div class='form-group'>\
                <label class='control-label col-sm-3' for='nom'>Nom</label> \
                <div class='col-sm-8'>\
                      <input class='form-control' type='text' name='nom' id='nom' pattern='[a-zA-Z]+' required>\
                </div>\
           </div>\
           <div class='form-group'>\
                <label class='control-label col-sm-3' for='prenom' >Prenom</label>\
                <div class='col-sm-8'>\
                     <input class='form-control' type='text' name='prenom' id='prenom' pattern='[a-zA-Z]+' required>\
                </div>\
           </div>\
           <div class='form-group'>\
                  <label class='control-label col-sm-3' for='pseudo'>Pseudo</label>\
               <div class='col-sm-8'>\
                   <input class='form-control' type='text' name='pseudo' id='pseudo' required>\
               </div>\
           </div>\
            <div class='form-group'>\
                  <label class='control-label col-sm-3' for='email'>Email</label>\
               <div class='col-sm-8'>\
                   <input class='form-control' type='email' name='email' id='email' pattern='^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$' required>\
               </div>\
           </div>\
           <div class='form-group'>\
                  <label class='control-label col-sm-3' for='mdp'>Mot de passe</label>\
               <div class='col-sm-8'>\
                   <input class='form-control' type='password' name='mdp' id='mdp' required>\
               </div>\
           </div>\
           <div class='form-group'>\
                  <label class='control-label col-sm-3' for='mdp2'>Confirmer le mot de passe</label>\
               <div class='col-sm-8'>\
                   <input class='form-control' type='password' name='mdp2' id='mdp2' required>\
               </div>\
           </div>\
        <input type='checkbox' value='cgu' name='cgu' required>  Cocher cette case pour accepter les <a href='index.php?module=CGU' target='_blank'>  condition d'utilisation</a><br/> \
        <input type='submit' class='btn btn-primary center-block' value='inscription'>\
      </form>");
}

function afficheBootboxDepotPhoto(token) {
    var b = bootbox.dialog({
            size :'medium',
            onEscape: true,
            backdrop:true,
            className:"",
            message:   " <div>\
                <div id='form' >\
                <h1>Déposer votre photo</h1>\
                <form class='form-horizontal' action='index.php?module=deposer' method='post' enctype='multipart/form-data' >\
                    <input type='hidden' name='token' value='"+token+"'>\
                    <div class='form-group'>\
                        <label class='control-label col-sm-3' for='titre'>Titre</label>\
                        <div class='col-sm-8'>\
                             <input class='form-control' type='text' name='titre' id='titre' required>\
                        </div>\
                    </div>\
                     <div style='margin-left: 15%' class='form-group'>\
                        <input type='file' name='fichImagePhoto'>\
                    </div>\
                     <input type='submit' class='btn btn-primary btn-responsive' value='deposer'>\
                 </form>\
                </div>\
             </div> "
        }
        , function(result) {
            if(result)
                $('#infos').submit();
        })
    b.find('.modal-content').addClass("col-ms-8");

}