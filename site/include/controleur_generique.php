<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 07/10/16
 * Time: 11:27
 */
class ControleurGenerique
{
    protected $modele;
    protected $vue;

    /**
     * @return mixed
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * @return mixed
     */
    public function getVue()
    {
        return $this->vue;
    }

    public function vue_erreur($msg){
        $this->vue->vue_erreur($msg);
    }
}
