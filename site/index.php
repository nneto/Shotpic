<?php
/**
 * Created by PhpStorm.
 * User: NICO
 * Date: 25/01/2017
 * Time: 11:13
 */

require_once('include/vue_generique.php');
require_once('include/controleur_generique.php');
require_once('include/modele_generique.php');
require_once('include/module_generique.php');
include("include/modele_generique_excepetion.php");
session_start();
$modGen = new ModeleGenerique();
$modGen::init();
if(isset($_GET['module'])) {
    $nom_module = $_GET['module'];
}else{
    $nom_module = "accueil";
}


switch ($nom_module) {
    case 'accueil':
        include_once('module/mod_accueil/mod_'.$nom_module.'.php');
        $nom_classe_module = "modaccueil";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_accueil($_GET['err']);
        }else{
            $module->module_accueil();
        }
        break;
    case 'inscription':
        include_once('module/mod_Inscription/mod_'.$nom_module.'.php');
        $nom_classe_module = "modinscription";
        $module = new $nom_classe_module();
        if(!(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            if (isset($_POST)&&isset($_POST['nom'])&& isset($_POST['prenom']) && isset($_POST['mdp']) && isset($_POST['mdp2']) && isset($_POST['pseudo'])  && isset($_POST['email'])&&isset($_POST['token'])){
                $module-> module_inscription($_POST);
            }else {
                header('Location:index.php?module=accueil&err=erreur');
            }
        }else {
            header('Location:index.php?module=accueil&err=vous%20etes%20deconnecter%20inscription');
        }
        break;
    case 'connexion':
        include_once ('module/mod_Connexion/mod_'.$nom_module.'.php');
        $nom_classe_module='modconnexion';
        $module=new $nom_classe_module();
        if(!(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            if(isset($_POST['pseudo'])&&isset($_POST['mdp'])&& isset($_POST['token'])){
                $module->module_connexion($_POST);
            }
        }else{
            header('Location:index.php?module=accueil&err=post');
        }
        break;
    case 'activer':
        include_once ('module/mod_activation/mod_'.$nom_module.'.php');
        $nom_classe_module='modactiver';
        $module=new $nom_classe_module();
        session_destroy();
        if(isset($_GET['token'])&&isset($_GET['idcompte'])){
            $module->activer($_GET['token'],$_GET['idcompte']);
        }else{
            header('Location:index.php?module=accueil');
        }
        break;
    case 'deconnexion':
        session_destroy();
        header('Location:index.php?module=accueil&err=vous%20etes%20deconnecter');
        break;
    case'affiche':
        include_once ('module/mod_affiche_photo/mod_affiche_photo.php');
        $nom_classe_module='ModuleAffichePhoto';
        $module=new $nom_classe_module;
        $module->afficher();
        break;
    case 'detail':
        include_once ('module/mod_details_photo/mod_details_photo.php');
        $nom_classe_module='ModDetailPhoto';
        $module=new $nom_classe_module;
        if(isset($_GET['id'])){
            $module->detail($_GET['id'] );
        }else{
            header('Location:index.php?module=accueil');
        }
        break;
    case 'CGU':
        include_once ('module/mod_cgu/module_cgu.php');
        $nom_classe_module='ModuleCgu';
        $module=new $nom_classe_module;
        $module->afficher();
        break;
    case 'deposer':
        include_once ('module/mod_ajout/mod_ajout.php');
        $nom_classe_module='ModuleAjoutPhoto';
        $module=new $nom_classe_module;
        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            if($_SESSION['admin']==1 || $_SESSION['modo']==1){
                header('Location:index.php?module=accueil');
            }else{
                if(isset($_POST['titre'])&&isset($_FILES['fichImagePhoto'])){
                    $module->afficher();
                }else{
                    header('Location:index.php?module=accueil&err=notPost');
                }
            }
        }else{
            header('Location:index.php?module=accueil&err=vous%20devez%20être%20connecter');
        }

        break;
    case 'mdp' :

        include_once ('module/mod_mdpOublier/module_mdp.php');
        $nom_classe_module='ModuleMdp';
        $module=new $nom_classe_module;
        if(!(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            if(isset($_POST['email'])&&isset($_POST['token'])) {
                $module->envoi($_POST);
            }elseif (isset($_GET['token'])&&isset($_GET['idcompte'])) {
                $module->formMdp2($_GET['token'], $_GET['idcompte']);
            }elseif (isset($_POST['mdp'])&&isset($_POST['mdp2'])&&isset($_POST['token'])){
                $module->change($_POST,$_GET['idcompte']);

            }else{
                $module->demande($_POST);
            }
        }else{
            session_destroy();
            header('Location:index.php?module=accueil&err=erreur%20404');
        }

        break;

    case 'ajouterCommentaire':
        include_once ('module/mod_ajouterCom/mod_ajoutCom.php');
        $nom_classe_module='ModuleAjoutCom';
        $module=new $nom_classe_module;
        if(isset($_POST['contenu'])&&isset($_POST['idphoto'])&&isset($_POST['token'])){
            $module->commenter($_POST);
        }else{
            header('Location:index.php?module=accueil&err=erreur%20Commentaire');
        }
        break;

    case 'profil':
        include_once ('module/mod_profil/mod_profil.php');
        $nom_classe_module='ModProfil';
        $module=new $nom_classe_module;
        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))) {
            if ($_SESSION['admin'] == 1) {
                header('Location:index.php?module=accueil');
            } else {

                if (isset($_POST['email']) && isset($_POST['token'])) {
                    $module->setEmail($_POST);
                } elseif (isset($_POST['mdp']) && isset($_POST['mdp2']) && isset($_POST['token'])) {
                    $module->setMdp($_POST);
                } else {
                    $module->afficherProfil();
                }
            }
        }
        break;
    case 'modifMdp':
        include_once ('module/mod_modifMdp/module_mdp.php');
        $nom_classe_module='ModuleMdp';
        $module=new $nom_classe_module;

        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            if(isset($_POST['mdp'])&& isset($_POST['mdp2'])&& isset($_POST['oldmdp']) && isset($_POST['token'])){
                $module->change($_POST);
            }else{
                $module->demande();
            }
        }else{
            header('Location:index.php?module=accueil&err=vous%20devez%20être%20connecter');
        }
        break;

    case 'modifEmail':

        include_once ('module/mod_modifEmail/module_ModifEmail.php');
        $module=new ModuleEmail();
        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))) {

            if(isset($_POST['email']) && isset($_POST['token'])){

                $module->change($_POST);
            }else{
                $module->demande();
            }
        }else{
            header('Location:index.php?module=accueil&err=vous%20devez%20être%20connecter');
        }
        break;
    case 'classement':
        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            if($_SESSION['admin']==1){
                include_once ('module/mod_classement/mod_classement.php');
                $nom_classe_module='ModClassement';
                $module=new $nom_classe_module;
                $module->afficherClassement();
            }else{
                header('Location:index.php?module=accueil');
            }
        }else{
            header('Location:index.php?module=accueil');
        }
        break;
    case 'modo':
        include_once ('module/mod_modo/mod_modo.php');
        $nom_classe_module='ModModo';
        $module=new $nom_classe_module;
        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            if($_SESSION['admin']==1){

                $module->afficherCompte();
            }

        }else{
            header('Location:index.php?module=accueil');
        }
        break;
    case 'delPhoto':
        include_once('module/mod_del_Photo/mod_del_Photo.php');
        $nom_classe_module='ModDelPhoto';
        $module=new $nom_classe_module;
        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            if($_SESSION['admin']==1){
                if(isset($_GET['id'])){
                    $module->module_delete($_GET['id']);
                }else{
                    header('Location:index.php?module=accueil');
                }

            }else{
                header('Location:index.php?module=accueil');
            }
        }
        break;

    case 'delCom':
        include_once('module/mod_del_Com/mod_del_Com.php');
        $nom_classe_module='ModDelCom';
        $module=new $nom_classe_module;
        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['pseudo'])&&isset($_SESSION['modo']) && isset($_SESSION['admin']))){
            if($_SESSION['admin']==1){
                if(isset($_GET['id'])){
                    $module->module_delete($_GET['id']);
                }else{
                    header('Location:index.php?module=accueil');
                }

            }else{
                header('Location:index.php?module=accueil');
            }
        }
        break;
    default:
        header('Location:index.php?module=accueil&err=erreur%20404%20erreur');

}
require_once('include/template.php');
