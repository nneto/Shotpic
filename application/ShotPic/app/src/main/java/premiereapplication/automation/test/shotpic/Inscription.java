package premiereapplication.automation.test.shotpic;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Vijay on 11/03/2017.
 */
public class Inscription extends AppCompatActivity implements View.OnClickListener,CompoundButton.OnCheckedChangeListener{
    private static  final  String url = "jdbc:mysql://sdigo.cf/projets4";
    private static  final String user = "youcef";
    private static final String psw = "benserida";
    private static final String salt="jnrth4r8t4a464erarzvsd";

    private Button bValider;
    private EditText etnom,etprenom,etpseudo,etmail,etmdp,etconfmdp;
    private CheckBox cocher;
    private TextInputLayout inputLayoutEmail;
    private static Connection conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);

        etnom=(EditText) findViewById(R.id.nom);
        etprenom=(EditText) findViewById(R.id.prenom);
        etpseudo=(EditText) findViewById(R.id.pseudo);
        etmail=(EditText) findViewById(R.id.email);
        etmdp=(EditText) findViewById(R.id.mdp);
        etconfmdp=(EditText) findViewById(R.id.mdp_conf);
        bValider=(Button) findViewById(R.id.valider);
        cocher=(CheckBox) findViewById(R.id.check);
        etmail.addTextChangedListener(new MyTextWatcher(etmail));

        bValider.setOnClickListener(this);
        cocher.setOnCheckedChangeListener(this);

        try {
            Class.forName("com.mysql.jdbc.Driver");
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "Problème au niveau du driver", Toast.LENGTH_SHORT).show();
        }

    }

    public void onCheckedChanged(CompoundButton buttonView,boolean isChecked){
        if(isChecked){
            cocher.setText("Vous acceptez les conditions d'utilisations");
        }else{
            cocher.setText("Vous n'acceptez pas les conditions d'utilisations");
        }
    }
    @Override
    public void onClick(View v){
        try{
            Connection conn = DriverManager.getConnection(url, user, psw);

            String sql ="INSERT INTO compte(email, motDePasse,pseudo,nom,prenom,modo,admin,activer )VALUES(?,SHA2(?,256) ,?,?,?,0,0,0)";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1,etmail.getText().toString());
            preparedStatement.setString(2, etmdp.getText().toString().concat(salt));
            preparedStatement.setString(3,etpseudo.getText().toString());
            preparedStatement.setString(4,etnom.getText().toString());
            preparedStatement.setString(5, etprenom.getText().toString());

            if(etpseudo.getText().toString().equals("") || etmail.getText().toString().equals("")||
                    etmdp.getText().toString().equals("")||etconfmdp.getText().toString().equals("")||
                    etnom.getText().toString().equals("")||etprenom.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Veuillez remplir tout les champs", Toast.LENGTH_SHORT).show();
            }else if (!etmdp.getText().toString().equals(etconfmdp.getText().toString())) {
                Toast.makeText(getApplicationContext(), "Les mots de passes sont différents", Toast.LENGTH_SHORT).show();
            }else if(!cocher.isChecked()){
                Toast.makeText(getApplicationContext(), "Vous n'avez pas acceptée les CGU, donc vous pouvez pas vous inscrire !", Toast.LENGTH_SHORT).show();
            }else{
                preparedStatement.executeUpdate();
                Toast.makeText(getApplicationContext(), "Vous vous êtes bien inscrit !", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Inscription.this, Connexion.class));
            }
        }catch (SQLException e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Un compte existe deja avec cette adresse mail et pseudo ", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validateEmail() {
        String email = etmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            inputLayoutEmail.setError("Entrez une adresse mail valide");
            requestFocus(etmail);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.email:
                    validateEmail();
                    break;
            }
        }
    }
}

