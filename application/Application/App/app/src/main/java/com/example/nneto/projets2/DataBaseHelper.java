package com.example.nneto.projets2;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
/**
 * Created by NNeto on 28/05/2016.
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    private static DataBaseHelper sInstance;
    private static int DATABASE_VERSION=1;
    private static String DATABASE_NAME="Reservation.db";

    public static final String PERSONNE_KEY="idPers";
    public static final String PERSONNE_TYPE="type";
    public static final String PERSONNE_NOM="nom";
    public static final String PERSONNE_PRENOM="prenom";
    public static final String PERSONNE_PSEUDO="pseudo";
    public static final String PERSONNE_MDP="MDP";
    public static final String PERSONNE_TABLE_NAME="Personne";

    public static final String MENU_KEY="idMenu";
    public static final String MENU_ALIMENT="aliment";
    public static final String MENU_DATE="DateMenu";
    public static final String MENU_TABLE_NAME="MENU";

    public static final String COMPTE_KEY_COMPTE="idCompte";
    public static final String COMPTE_KEY_CONSO="idConsommateur";
    public static final String COMPTE_KEY_CREDIT="idCrediteur";
    public static final String COMPTE_SOLDE="Solde";
    public static final String COMPTE_TABLE_NAME="Compte";

    public static final String RESERVATION_KEY="idReserv";
    public static final String RESERVATION_DATE="DateReserv";
    public static final String RESERVATION_HORRAIRE_DEBUT="plageHorraireDebut";
    public static final String RESERVATION_HORRAIRE_FIN="plageHorraireFin";
    public static final String RESERVATION_TABLE_NAME="Reservation";
    public static final String RESERVATION_MENU="idMenu";
    public static final String RESERVATION_PERSONNE="idPers";

    public static final String PERSONNE_TABLE_CREATE=
            "TABLE CREATE"+PERSONNE_TABLE_NAME+"("+
            PERSONNE_KEY+"INTEGER PRIMARY KEY AUTOINCREMENT,"+
            PERSONNE_PSEUDO+"TEXT PRIMARY KEY,"+
            PERSONNE_NOM+"TEXT,"+
            PERSONNE_PRENOM+"TEXT,"+
            PERSONNE_TYPE+"INTEGER,"+
            PERSONNE_MDP+"TEXT);"
            ;

    public static final String MENU_TABLE_CREATE=
            "TABLE CREATE"+MENU_TABLE_NAME+"("+
             MENU_KEY+"INTEGER PRIMARY KEY AUTOINCREMENT,"+
             MENU_ALIMENT+"TEXT,"+
             MENU_DATE+"DATE);";

    public static final String COMPTE_TABLE_CREATE=
            "TABLE CREATE"+COMPTE_TABLE_NAME+"("+
             COMPTE_KEY_COMPTE+"INTEGER PRIMARY KEY AUTOINCREMENT,"+
              COMPTE_KEY_CONSO+"INTEGER,"+
              COMPTE_KEY_CREDIT+"INTEGER,"+
              COMPTE_SOLDE+"REAL,"+
              "FOREIGN KEY("+COMPTE_KEY_CONSO+")REFERENCES"+PERSONNE_TABLE_NAME+"("+PERSONNE_KEY+"));";

    public static final String RESERVATION_TABLE_CREATE=
            "TABLE CREATE"+RESERVATION_TABLE_NAME+"("+
             RESERVATION_KEY+"INTEGER PRIMARY KEY AUTOINCREMENT,"+
             RESERVATION_HORRAIRE_DEBUT+"TIME,"+
             RESERVATION_HORRAIRE_FIN+"TIME,"+
             RESERVATION_DATE+"DATE,"+
             RESERVATION_MENU+"INTEGER,"+
             RESERVATION_PERSONNE+"INTEGER,"+
             "FOREIGN KEY("+RESERVATION_PERSONNE+")REFERENCES"+PERSONNE_TABLE_NAME+"("+PERSONNE_KEY+")),"+
            "FOREIGN KEY("+RESERVATION_MENU+")REFERENCES"+MENU_TABLE_NAME+"("+MENU_KEY+"));";

    public static final String DROP="DROP TABLE IF EXISTS"+COMPTE_TABLE_NAME+";" +
            "DROP TABLE IF EXISTS" +RESERVATION_TABLE_NAME+";"+
            "DROP TABLE IF EXISTS"+PERSONNE_TABLE_NAME+";"+
            "DROP TABLE IF EXISTS"+MENU_TABLE_NAME+";";


    private DataBaseHelper (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PERSONNE_TABLE_CREATE);
        db.execSQL(COMPTE_TABLE_CREATE);
        db.execSQL(MENU_TABLE_CREATE);
        db.execSQL(RESERVATION_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP);
        onCreate(db);
    }

    public static synchronized DataBaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new  DataBaseHelper (context);
        }
        return sInstance;
    }
}
