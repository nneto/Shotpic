package com.example.nneto.projets2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
public class MenuView extends Activity {

    private static  final  String url = "jdbc:mysql://hostinger.fr/u502877693_proj";
    private static  final String user = "u502877693_admin";
    private static final String psw = "UOJ5ZJxHeU";
    private ImageButton reglage =null;
    private ListView m = null;
    private TextView date = null;
    private TextView entree = null;
    private TextView plat = null;
    private TextView dessert = null;
    private static  Activity a= null;
    private Button reserver = null;
    private boolean estReserver = false;
    Date d = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        a=this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        reserver =(Button)findViewById(R.id.reserve);
        date= (TextView) findViewById(R.id.Date);
        entree= (TextView) findViewById(R.id.Entree);
        plat= (TextView)findViewById(R.id.Plat);
        dessert= (TextView)findViewById(R.id.Dessert);
        reglage= (ImageButton) findViewById(R.id.engrenage);
        reglage.setOnClickListener(reglageListerner);
        reserver.setOnClickListener(reserverListener);
    //     SimpleDateFormat f =new SimpleDateFormat("yyyy-MM-dd");
      /*Incrementation de Date
                Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE,1);
        recupBD();
        String tomorow = (String)(f.format(c.getTime()));

         */
        d = new Date();

    }

    private void recupBD() {
        try{
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, user, psw);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select Entree from Menu where "+d+ " = DateMenu ");
            entree.setText(rs.getString("Entree"));
            rs=st.executeQuery("select Plat from Menu where "+d+ " = DateMenu ");
            plat.setText(rs.getString("Plat"));
            rs=st.executeQuery("select Dessert from Menu where "+d+ " = DateMenu ");
            dessert.setText(rs.getString("Dessert"));
            rs=st.executeQuery("select * from reservation natural join Personne  where pseudo = "+Connexion.getLogin());
            if(rs != null){

            }
        }catch(Exception e){
            Toast.makeText(getApplicationContext(),"Probleme Connexion BD",Toast.LENGTH_SHORT).show();
        }

    }

    public static Activity getA(){
        return a ;
    }

    private View.OnClickListener reglageListerner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent reglage= new Intent(MenuView.this,Reglage.class);
            startActivity(reglage);

        }

    };

    private View.OnClickListener reserverListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(estReserver){
                reserver.setText("Reserver");
                reserver.setBackgroundColor(getResources().getColor(R.color.grey));
                estReserver=false;
            }else{
                reserver.setText("Annuler");
                reserver.setBackgroundColor(getResources().getColor(R.color.red));
                estReserver=true;
            }
        }
    };
}
