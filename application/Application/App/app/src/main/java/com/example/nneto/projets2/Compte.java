package com.example.nneto.projets2;

/**
 * Created by NNeto on 28/05/2016.
 */
public class Compte {
    private long idCompte;
    private long idConsommateur;
    private long idCrediteur;
    private double Solde;

    public Compte(long idCompte, long idConsommateur, long idCrediteur, double solde) {
        this.idCompte = idCompte;
        this.idConsommateur = idConsommateur;
        this.idCrediteur = idCrediteur;
        Solde = solde;
    }

    public long getIdCompte() {
        return idCompte;
    }

    public void setIdCompte(long idCompte) {
        this.idCompte = idCompte;
    }

    public long getIdConsommateur() {
        return idConsommateur;
    }

    public void setIdConsommateur(long idConsommateur) {
        this.idConsommateur = idConsommateur;
    }

    public long getIdCrediteur() {
        return idCrediteur;
    }

    public void setIdCrediteur(long idCrediteur) {
        this.idCrediteur = idCrediteur;
    }

    public double getSolde() {
        return Solde;
    }

    public void setSolde(double solde) {
        Solde = solde;
    }
}
