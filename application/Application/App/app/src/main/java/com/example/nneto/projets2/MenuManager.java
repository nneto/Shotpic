package com.example.nneto.projets2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by NNeto on 31/05/2016.
 */
public class MenuManager {

    public static final String MENU_KEY="idMenu";
    public static final String MENU_ALIMENT="aliment";
    public static final String MENU_DATE="DateMenu";
    public static final String TABLE_NAME="MENU";

    private DataBaseHelper baseSQLite;
    private SQLiteDatabase db;

    public MenuManager (Context c ){
        baseSQLite = DataBaseHelper.getInstance(c);
    }

    public void open(){
        db=baseSQLite.getWritableDatabase();
    }

    public void close(){
        db.close();
    }

    public long add (Menu m){
        ContentValues values = new ContentValues();

        values.put(MENU_ALIMENT,m.getAliment());
        values.put(MENU_DATE, String.valueOf(m.getDate()));

        return db.insert(TABLE_NAME,null,values);
    }

    public int mod (Menu m){
        ContentValues values = new ContentValues();
        values.put(MENU_ALIMENT,m.getAliment());
        values.put(MENU_DATE, String.valueOf(m.getDate()));

        String where = MENU_KEY+"= ?";
        String[]whereArgs   ={m.getId()+""};

        return db.update(TABLE_NAME,values,where,whereArgs);
    }

    public int sup (Menu m){
        String where = MENU_KEY+"= ?";
        String[]whereArgs   ={m.getId()+""};

        return db.delete(TABLE_NAME,where,whereArgs);
    }

    public Menu getMenu (long id){
        Menu m = new Menu(0,"",null);
        Cursor c = db.rawQuery("Select * from "+TABLE_NAME+" where "+ MENU_KEY + " = "+id,null);
        if (c.moveToFirst()){
            m.setId(c.getInt(c.getColumnIndex(MENU_KEY)));
            m.setAliment(c.getString(c.getColumnIndex(MENU_ALIMENT)));
            /*
            m.setDate(c.getInt(Integer.parseInt(MENU_DATE)));*/
            c.close();
        }
        return m;
    }
    public Cursor getAllMenus(){
        return  db.rawQuery("Select * from" +TABLE_NAME,null);
    }

}
