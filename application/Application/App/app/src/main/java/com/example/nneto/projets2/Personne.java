package com.example.nneto.projets2;

/**
 * Created by NNeto on 28/05/2016.
 */
public class Personne {
    private long id ;
    private int type;
    private String nom;
    private String prenom;
    private String pseudo;
    private String MDP;

    public Personne(long id, int type, String nom, String prenom, String pseudo, String MDP) {
        this.id = id;
        this.type = type;
        this.nom = nom;

        this.prenom = prenom;
        this.pseudo = pseudo;
        this.MDP = MDP;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMDP() {
        return MDP;
    }

    public void setMDP(String MDP) {
        this.MDP = MDP;
    }


}
