package com.example.nneto.projets2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by NNeto on 30/05/2016.
 */
public class PersonneManager {

    public static final String PERSONNE_KEY="idPers";
    public static final String PERSONNE_TYPE="type";
    public static final String PERSONNE_NOM="nom";
    public static final String PERSONNE_PRENOM="prenom";
    public static final String PERSONNE_PSEUDO="pseudo";
    public static final String PERSONNE_MDP="MDP";
    public static final String TABLE_NAME="Personne";

    private DataBaseHelper baseSQLite;
    private SQLiteDatabase db;

    public PersonneManager (Context c){
        baseSQLite=DataBaseHelper.getInstance(c);
    }

    public void open(){
        db=baseSQLite.getWritableDatabase();
    }

    public void close(){
        db.close();
    }

    public long add (Personne p){

        ContentValues values = new ContentValues();
        values.put(PERSONNE_PSEUDO,p.getPseudo());
        values.put(PERSONNE_NOM,p.getNom());
        values.put(PERSONNE_PRENOM,p.getPrenom());
        values.put(PERSONNE_TYPE,p.getType());
        values.put(PERSONNE_MDP,p.getMDP());
        return db.insert(TABLE_NAME,null,values);
    }

    public int mod(Personne p){
       ContentValues values = new ContentValues();
        values.put(PERSONNE_PSEUDO,p.getPseudo());
        values.put(PERSONNE_NOM,p.getNom());
        values.put(PERSONNE_PRENOM,p.getPrenom());
        values.put(PERSONNE_TYPE,p.getType());
        values.put(PERSONNE_MDP,p.getMDP());

        String where = PERSONNE_KEY+"= ?";
        String[]whereArgs   ={p.getId()+""};

        return db.update(TABLE_NAME,values,where,whereArgs);
    }

    public int sup (Personne p){
        String where = PERSONNE_KEY+"= ?";
        String[]whereArgs   ={p.getId()+""};

        return db.delete(TABLE_NAME,where,whereArgs);
    }

    public Personne getPersonne (long id , String pseudo){
        Personne p = new Personne(0,0,"","","","");
        Cursor c = db.rawQuery("Select * from " + TABLE_NAME + " Where "+ PERSONNE_KEY + " = "+id+" and "+PERSONNE_PSEUDO + " = "+ pseudo,null);
        if(c.moveToFirst()){
            p.setId(c.getInt(c.getColumnIndex(PERSONNE_KEY)));
            p.setPseudo(c.getString(c.getColumnIndex(PERSONNE_PSEUDO)));
            p.setNom(c.getString(c.getColumnIndex(PERSONNE_NOM)));
            p.setPrenom(c.getString(c.getColumnIndex(PERSONNE_PRENOM)));
            p.setType(c.getInt(c.getColumnIndex(PERSONNE_TYPE)));
            p.setMDP(c.getString(c.getColumnIndex(PERSONNE_MDP)));
            c.close();
        }
        return  p;
    }

    public Cursor getAllPersonnes(){
        return  db.rawQuery("Select * from" +TABLE_NAME,null);
    }

}
