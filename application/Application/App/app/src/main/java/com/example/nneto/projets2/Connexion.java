package com.example.nneto.projets2;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


/**
 * Created by NNeto on 24/05/2016.
 */

public class Connexion extends Activity  {
//"jdbc:mysql://mysql.hostinger.fr/u502877693_proj"
    private Button connexionButton = null;
    private  static EditText login = null;
    private EditText mdp = null;
    private SQLiteDatabase db = null;
    private static  final  String url = "jdbc:mysql://hostinger.fr/u502877693_proj";
    private static  final String user = "u502877693_admin";
    private static final String psw = "UOJ5ZJxHeU";
    private static String dbName = "u502877693_proj";
    @Override
    protected void onCreate(Bundle savedInstanceState)  {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.connexion);
        connexionButton = (Button) findViewById(R.id.button);
        login = (EditText) findViewById(R.id.editLogin);
        mdp = (EditText) findViewById(R.id.editMDP);
        connexionButton.setOnClickListener(connexionListener);

    }

    private View.OnClickListener connexionListener = new View.OnClickListener() {
        @Override
        public void onClick(View v)  {
//
            if ((login.getText().toString().equals("")) || (mdp.getText().toString().equals(""))) {
                Toast.makeText(getApplicationContext(), "login ou mot de passe vide", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    connexionDb();
                    Intent i = new Intent(Connexion.this,MenuView.class);
                    startActivity(i);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Probleme de connexion " + e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }

    };

    public void connexionDb() throws Exception {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Class.forName("com.mysql.jdbc.Driver");
        Toast.makeText(getApplicationContext(), "Driver ok", Toast.LENGTH_LONG).show();
        Connection con = DriverManager.getConnection(url, user, psw);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("select * from Personne where "+login +" = pseudo and "+mdp+" = MDP");


    }

    public static String getLogin() {
        return login.getText().toString();
    }
}