package com.example.nneto.projets2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by NNeto on 01/06/2016.
 */
public class ReservationManager {
    public static final String RESERVATION_KEY="idReserv";
    public static final String RESERVATION_DATE="DateReserv";
    public static final String RESERVATION_HORRAIRE_DEBUT="plageHorraireDebut";
    public static final String RESERVATION_HORRAIRE_FIN="plageHorraireFin";
    public static final String TABLE_NAME="Reservation";
    public static final String RESERVATION_MENU="idMenu";
    public static final String RESERVATION_PERSONNE="idPers";

    private DataBaseHelper baseSQLite;
    private SQLiteDatabase db;

    public ReservationManager (Context c){
        baseSQLite=DataBaseHelper.getInstance(c);
    }

    public void open(){
        db=baseSQLite.getWritableDatabase();
    }

    public void close(){
        db.close();
    }

    public long add (Reservation r){
        ContentValues values = new ContentValues();
        values.put(RESERVATION_DATE, String.valueOf(r.getDate()));
        values.put(RESERVATION_HORRAIRE_DEBUT, String.valueOf(r.getPlageHorraireDebut()));
        values.put(RESERVATION_HORRAIRE_FIN, String.valueOf(r.getPlageHorraireFin()));
        values.put(RESERVATION_MENU,r.getIdMenu());
        values.put(RESERVATION_PERSONNE,r.getIdPers());
        return db.insert(TABLE_NAME,null,values);
    }

    public int mod (Reservation r){
        ContentValues values = new ContentValues();
        values.put(RESERVATION_DATE, String.valueOf(r.getDate()));
        values.put(RESERVATION_HORRAIRE_DEBUT, String.valueOf(r.getPlageHorraireDebut()));
        values.put(RESERVATION_HORRAIRE_FIN, String.valueOf(r.getPlageHorraireFin()));
        values.put(RESERVATION_MENU,r.getIdMenu());
        values.put(RESERVATION_PERSONNE,r.getIdPers());

        String where = RESERVATION_KEY+"= ?";
        String[]whereArgs   ={r.getId()+""};

        return db.update(TABLE_NAME,values,where,whereArgs);
    }

    public int sup (Personne p){
        String where = RESERVATION_KEY+"= ?";
        String[]whereArgs   ={p.getId()+""};

        return db.delete(TABLE_NAME,where,whereArgs);
    }

    public Reservation getReservation(long id){
        Reservation r = new Reservation(0,null,null,null,0,0);
        Cursor c = db.rawQuery("Select * from " + TABLE_NAME + "Where"+ RESERVATION_KEY,null);

        if (c.moveToFirst()){
            r.setId(c.getColumnIndex(RESERVATION_KEY));
           // r.setDate(c.getColumnIndex(RESERVATION_DATE));
           // r.setPlageHorraireDebut(c.getColumnIndex(RESERVATION_HORRAIRE_DEBUT));
          //r.setPlageHorraireFin(c.getColumnIndex(RESERVATION_HORRAIRE_FIN));
            r.setIdMenu(c.getColumnIndex(RESERVATION_MENU));
            r.setIdPers(c.getColumnIndex(RESERVATION_PERSONNE));

            c.close();
        }
        return  r;
    }
    public Cursor getAllPersonnes(){
        return  db.rawQuery("Select * from" +TABLE_NAME,null);
    }


}
