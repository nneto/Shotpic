package com.example.nneto.projets2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by NNeto on 24/05/2016.
 */

public class Reglage extends Activity {
    private Button changer= null;
    private Button deco = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reglage);
        changer = (Button) findViewById(R.id.changer);
        deco = (Button) findViewById(R.id.deco);
        deco.setOnClickListener(decoListenner);
        changer.setOnClickListener(changerListener);
    }
    private View.OnClickListener changerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MenuView.getA().finish();
            Intent i = new Intent(Reglage.this,ChangerMDP.class);
            startActivity(i);
        }
    };


    private View.OnClickListener decoListenner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        MenuView.getA().finish();
            Reglage.this.finish();
        }
    };

}
