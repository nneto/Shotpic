package com.example.nneto.projets2;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;

/**
 * Created by NNeto on 15/06/2016.
 */
public class SigninActivity extends AsyncTask<String,Void,String> {
    private Context context;

    public SigninActivity(Context c) {
        this.context = c;
    }

    @Override
    protected String doInBackground(String... params) {
        try {

            String username = (String) params[0];
            String password = (String) params[1];
            String link = "http://projetiut2016.esy.es/functionsPHP/connextion_function.php";

            URL url = new URL(link);
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet();
            request.setURI(new URI(link));
            HttpResponse response = client.execute(request);
            BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer sb = new StringBuffer("");
            String line = "";

            while ((line = in.readLine()) != null) {
                sb.append(line);
                break;
            }
            in.close();
            return sb.toString();
        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());
        }

    }

}