package com.example.nneto.projets2;

import java.sql.Date;

/**
 * Created by NNeto on 28/05/2016.
 */
public class Menu {
    private long id ;
    private String aliment;
    private Date date;

    public Menu(long id, String aliment, Date date) {
        this.id = id;
        this.aliment = aliment;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAliment() {
        return aliment;
    }

    public void setAliment(String aliment) {
        this.aliment = aliment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
