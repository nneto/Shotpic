package com.example.nneto.projets2;

import java.sql.Date;
import java.sql.Time;

/**
 * Created by NNeto on 28/05/2016.
 */
public class Reservation {
    private long id;
    private Date date;
    private Time plageHorraireDebut;
    private Time plageHorraireFin;
    private long idMenu;
    private long idPers;

    public Reservation(long id, Date date, Time plageHorraireDebut, Time plageHorraireFin, long idMenu, long idPers) {
        this.id = id;
        this.date = date;
        this.plageHorraireDebut = plageHorraireDebut;
        this.plageHorraireFin = plageHorraireFin;
        this.idMenu = idMenu;
        this.idPers = idPers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getPlageHorraireDebut() {
        return plageHorraireDebut;
    }

    public void setPlageHorraireDebut(Time plageHorraireDebut) {
        this.plageHorraireDebut = plageHorraireDebut;
    }

    public Time getPlageHorraireFin() {
        return plageHorraireFin;
    }

    public void setPlageHorraireFin(Time plageHorraireFin) {
        this.plageHorraireFin = plageHorraireFin;
    }

    public long getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(long idMenu) {
        this.idMenu = idMenu;
    }

    public long getIdPers() {
        return idPers;
    }

    public void setIdPers(long idPers) {
        this.idPers = idPers;
    }
}
