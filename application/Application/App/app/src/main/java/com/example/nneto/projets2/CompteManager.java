package com.example.nneto.projets2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
/**
 * Created by NNeto on 29/05/2016.
 */
public class CompteManager {

    private static final  String TABLE_NAME="Compte";
    public static final String COMPTE_KEY_COMPTE="idCompte";
    public static final String COMPTE_KEY_CONSO="idConsommateur";
    public static final String COMPTE_KEY_CREDIT="idCrediteur";
    public static final String COMPTE_SOLDE="Solde";

    private DataBaseHelper baseSQLite;
    private SQLiteDatabase db;

    public CompteManager(Context c)
    {
        baseSQLite= DataBaseHelper.getInstance(c);
    }

    public void open(){

        db=baseSQLite.getWritableDatabase();
    }

    public void close(){
        db.close();
    }

    public long add (Compte c){
        ContentValues values= new ContentValues();
        values.put(COMPTE_KEY_CONSO,c.getIdConsommateur());
        values.put(COMPTE_KEY_CREDIT,c.getIdCrediteur());
        values.put(COMPTE_SOLDE,c.getSolde());
        return db.insert(TABLE_NAME,null,values);
    }

    public int mod (Compte c){
        ContentValues values= new ContentValues();
        values.put(COMPTE_KEY_CONSO,c.getIdConsommateur());
        values.put(COMPTE_KEY_CREDIT,c.getIdCrediteur());
        values.put(COMPTE_SOLDE,c.getSolde());

        String where = COMPTE_KEY_COMPTE+"= ?";
        String[]whereArgs   ={c.getIdCompte()+""};

        return db.update(TABLE_NAME,values,where,whereArgs);
    }

    public int sup(Compte c){
        String where = COMPTE_KEY_COMPTE+"= ?";
        String[]whereArgs   ={c.getIdCompte()+""};

       return db.delete(TABLE_NAME,where,whereArgs);
    }

    public Compte getCompte (long id){
        Compte c= new Compte(0,0,0,0);
        Cursor curs= db.rawQuery("Select * From "+ TABLE_NAME+" WHERE "+COMPTE_KEY_COMPTE+" = "+id,null);
        if(curs.moveToFirst()){
            c.setIdCompte(curs.getInt(curs.getColumnIndex(COMPTE_KEY_COMPTE)));
            c.setIdConsommateur(curs.getInt(curs.getColumnIndex(COMPTE_KEY_CONSO)));
            c.setIdCrediteur(curs.getInt(curs.getColumnIndex(COMPTE_KEY_CREDIT)));
            c.setSolde(curs.getDouble(curs.getColumnIndex(COMPTE_SOLDE)));
            curs.close();
        }
        return c;
    }

    public Cursor getAllComptes(){return db.rawQuery("Select * from" +TABLE_NAME,null);}
}
